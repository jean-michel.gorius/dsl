/*
 * generated by Xtext 2.15.0
 */
grammar InternalSat;

options {
	superClass=AbstractInternalContentAssistParser;
}

@lexer::header {
package org.sif.dsl.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package org.sif.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.sif.dsl.services.SatGrammarAccess;

}
@parser::members {
	private SatGrammarAccess grammarAccess;

	public void setGrammarAccess(SatGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}

	@Override
	protected Grammar getGrammar() {
		return grammarAccess.getGrammar();
	}

	@Override
	protected String getValueForTokenName(String tokenName) {
		return tokenName;
	}
}

// Entry rule entryRuleStart
entryRuleStart
:
{ before(grammarAccess.getStartRule()); }
	 ruleStart
{ after(grammarAccess.getStartRule()); } 
	 EOF 
;

// Rule Start
ruleStart 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getStartAccess().getGroup()); }
		(rule__Start__Group__0)
		{ after(grammarAccess.getStartAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleCheck
entryRuleCheck
:
{ before(grammarAccess.getCheckRule()); }
	 ruleCheck
{ after(grammarAccess.getCheckRule()); } 
	 EOF 
;

// Rule Check
ruleCheck 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getCheckAccess().getGroup()); }
		(rule__Check__Group__0)
		{ after(grammarAccess.getCheckAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleSatSolver
entryRuleSatSolver
:
{ before(grammarAccess.getSatSolverRule()); }
	 ruleSatSolver
{ after(grammarAccess.getSatSolverRule()); } 
	 EOF 
;

// Rule SatSolver
ruleSatSolver 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getSatSolverAccess().getAlternatives()); }
		(rule__SatSolver__Alternatives)
		{ after(grammarAccess.getSatSolverAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleBiimplies
entryRuleBiimplies
:
{ before(grammarAccess.getBiimpliesRule()); }
	 ruleBiimplies
{ after(grammarAccess.getBiimpliesRule()); } 
	 EOF 
;

// Rule Biimplies
ruleBiimplies 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getBiimpliesAccess().getGroup()); }
		(rule__Biimplies__Group__0)
		{ after(grammarAccess.getBiimpliesAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleImplies
entryRuleImplies
:
{ before(grammarAccess.getImpliesRule()); }
	 ruleImplies
{ after(grammarAccess.getImpliesRule()); } 
	 EOF 
;

// Rule Implies
ruleImplies 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getImpliesAccess().getGroup()); }
		(rule__Implies__Group__0)
		{ after(grammarAccess.getImpliesAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleOr
entryRuleOr
:
{ before(grammarAccess.getOrRule()); }
	 ruleOr
{ after(grammarAccess.getOrRule()); } 
	 EOF 
;

// Rule Or
ruleOr 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getOrAccess().getGroup()); }
		(rule__Or__Group__0)
		{ after(grammarAccess.getOrAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleExcludes
entryRuleExcludes
:
{ before(grammarAccess.getExcludesRule()); }
	 ruleExcludes
{ after(grammarAccess.getExcludesRule()); } 
	 EOF 
;

// Rule Excludes
ruleExcludes 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getExcludesAccess().getGroup()); }
		(rule__Excludes__Group__0)
		{ after(grammarAccess.getExcludesAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleAnd
entryRuleAnd
:
{ before(grammarAccess.getAndRule()); }
	 ruleAnd
{ after(grammarAccess.getAndRule()); } 
	 EOF 
;

// Rule And
ruleAnd 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getAndAccess().getGroup()); }
		(rule__And__Group__0)
		{ after(grammarAccess.getAndAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleNot
entryRuleNot
:
{ before(grammarAccess.getNotRule()); }
	 ruleNot
{ after(grammarAccess.getNotRule()); } 
	 EOF 
;

// Rule Not
ruleNot 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getNotAccess().getGroup()); }
		(rule__Not__Group__0)
		{ after(grammarAccess.getNotAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRulePrimary
entryRulePrimary
:
{ before(grammarAccess.getPrimaryRule()); }
	 rulePrimary
{ after(grammarAccess.getPrimaryRule()); } 
	 EOF 
;

// Rule Primary
rulePrimary 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getPrimaryAccess().getAlternatives()); }
		(rule__Primary__Alternatives)
		{ after(grammarAccess.getPrimaryAccess().getAlternatives()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleVariable
entryRuleVariable
:
{ before(grammarAccess.getVariableRule()); }
	 ruleVariable
{ after(grammarAccess.getVariableRule()); } 
	 EOF 
;

// Rule Variable
ruleVariable 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getVariableAccess().getIdAssignment()); }
		(rule__Variable__IdAssignment)
		{ after(grammarAccess.getVariableAccess().getIdAssignment()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRulePFormula
entryRulePFormula
:
{ before(grammarAccess.getPFormulaRule()); }
	 rulePFormula
{ after(grammarAccess.getPFormulaRule()); } 
	 EOF 
;

// Rule PFormula
rulePFormula 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getPFormulaAccess().getGroup()); }
		(rule__PFormula__Group__0)
		{ after(grammarAccess.getPFormulaAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SatSolver__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSatSolverAccess().getNameAssignment_0()); }
		(rule__SatSolver__NameAssignment_0)
		{ after(grammarAccess.getSatSolverAccess().getNameAssignment_0()); }
	)
	|
	(
		{ before(grammarAccess.getSatSolverAccess().getNameAssignment_1()); }
		(rule__SatSolver__NameAssignment_1)
		{ after(grammarAccess.getSatSolverAccess().getNameAssignment_1()); }
	)
	|
	(
		{ before(grammarAccess.getSatSolverAccess().getNameAssignment_2()); }
		(rule__SatSolver__NameAssignment_2)
		{ after(grammarAccess.getSatSolverAccess().getNameAssignment_2()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Alternatives_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getAndAccess().getNotParserRuleCall_0_0()); }
		ruleNot
		{ after(grammarAccess.getAndAccess().getNotParserRuleCall_0_0()); }
	)
	|
	(
		{ before(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0_1()); }
		rulePrimary
		{ after(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0_1()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__And__RightAlternatives_1_2_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0_0()); }
		ruleNot
		{ after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0_0()); }
	)
	|
	(
		{ before(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0_1()); }
		rulePrimary
		{ after(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0_1()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Primary__Alternatives
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getPrimaryAccess().getVariableParserRuleCall_0()); }
		ruleVariable
		{ after(grammarAccess.getPrimaryAccess().getVariableParserRuleCall_0()); }
	)
	|
	(
		{ before(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1()); }
		rulePFormula
		{ after(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Start__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Start__Group__0__Impl
	rule__Start__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Start__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStartAccess().getCheckParserRuleCall_0()); }
	ruleCheck
	{ after(grammarAccess.getStartAccess().getCheckParserRuleCall_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Start__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Start__Group__1__Impl
	rule__Start__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Start__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStartAccess().getStartChecksAction_1()); }
	()
	{ after(grammarAccess.getStartAccess().getStartChecksAction_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Start__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Start__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Start__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStartAccess().getChecksAssignment_2()); }
	(rule__Start__ChecksAssignment_2)*
	{ after(grammarAccess.getStartAccess().getChecksAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Check__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Check__Group__0__Impl
	rule__Check__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCheckAccess().getCheckKeyword_0()); }
	'Check'
	{ after(grammarAccess.getCheckAccess().getCheckKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Check__Group__1__Impl
	rule__Check__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCheckAccess().getFormulaAssignment_1()); }
	(rule__Check__FormulaAssignment_1)
	{ after(grammarAccess.getCheckAccess().getFormulaAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Check__Group__2__Impl
	rule__Check__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCheckAccess().getUsingKeyword_2()); }
	'using'
	{ after(grammarAccess.getCheckAccess().getUsingKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Check__Group__3__Impl
	rule__Check__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCheckAccess().getSolverAssignment_3()); }
	(rule__Check__SolverAssignment_3)
	{ after(grammarAccess.getCheckAccess().getSolverAssignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Check__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getCheckAccess().getSemicolonKeyword_4()); }
	';'
	{ after(grammarAccess.getCheckAccess().getSemicolonKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Biimplies__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Biimplies__Group__0__Impl
	rule__Biimplies__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); }
	ruleImplies
	{ after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Biimplies__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBiimpliesAccess().getGroup_1()); }
	(rule__Biimplies__Group_1__0)*
	{ after(grammarAccess.getBiimpliesAccess().getGroup_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Biimplies__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Biimplies__Group_1__0__Impl
	rule__Biimplies__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); }
	()
	{ after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Biimplies__Group_1__1__Impl
	rule__Biimplies__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBiimpliesAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1()); }
	'<=>'
	{ after(grammarAccess.getBiimpliesAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Biimplies__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); }
	(rule__Biimplies__RightAssignment_1_2)
	{ after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Implies__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Implies__Group__0__Impl
	rule__Implies__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImpliesAccess().getOrParserRuleCall_0()); }
	ruleOr
	{ after(grammarAccess.getImpliesAccess().getOrParserRuleCall_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Implies__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImpliesAccess().getGroup_1()); }
	(rule__Implies__Group_1__0)*
	{ after(grammarAccess.getImpliesAccess().getGroup_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Implies__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Implies__Group_1__0__Impl
	rule__Implies__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); }
	()
	{ after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Implies__Group_1__1__Impl
	rule__Implies__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImpliesAccess().getEqualsSignGreaterThanSignKeyword_1_1()); }
	'=>'
	{ after(grammarAccess.getImpliesAccess().getEqualsSignGreaterThanSignKeyword_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Implies__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); }
	(rule__Implies__RightAssignment_1_2)
	{ after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Or__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Or__Group__0__Impl
	rule__Or__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getOrAccess().getExcludesParserRuleCall_0()); }
	ruleExcludes
	{ after(grammarAccess.getOrAccess().getExcludesParserRuleCall_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Or__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getOrAccess().getGroup_1()); }
	(rule__Or__Group_1__0)*
	{ after(grammarAccess.getOrAccess().getGroup_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Or__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Or__Group_1__0__Impl
	rule__Or__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); }
	()
	{ after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Or__Group_1__1__Impl
	rule__Or__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getOrAccess().getOrKeyword_1_1()); }
	'or'
	{ after(grammarAccess.getOrAccess().getOrKeyword_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Or__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getOrAccess().getRightAssignment_1_2()); }
	(rule__Or__RightAssignment_1_2)
	{ after(grammarAccess.getOrAccess().getRightAssignment_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Excludes__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Excludes__Group__0__Impl
	rule__Excludes__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getExcludesAccess().getAndParserRuleCall_0()); }
	ruleAnd
	{ after(grammarAccess.getExcludesAccess().getAndParserRuleCall_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Excludes__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getExcludesAccess().getGroup_1()); }
	(rule__Excludes__Group_1__0)*
	{ after(grammarAccess.getExcludesAccess().getGroup_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Excludes__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Excludes__Group_1__0__Impl
	rule__Excludes__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); }
	()
	{ after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Excludes__Group_1__1__Impl
	rule__Excludes__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getExcludesAccess().getNandKeyword_1_1()); }
	'nand'
	{ after(grammarAccess.getExcludesAccess().getNandKeyword_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Excludes__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); }
	(rule__Excludes__RightAssignment_1_2)
	{ after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__And__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__And__Group__0__Impl
	rule__And__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getAndAccess().getAlternatives_0()); }
	(rule__And__Alternatives_0)
	{ after(grammarAccess.getAndAccess().getAlternatives_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__And__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getAndAccess().getGroup_1()); }
	(rule__And__Group_1__0)*
	{ after(grammarAccess.getAndAccess().getGroup_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__And__Group_1__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__And__Group_1__0__Impl
	rule__And__Group_1__1
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group_1__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); }
	()
	{ after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group_1__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__And__Group_1__1__Impl
	rule__And__Group_1__2
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group_1__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getAndAccess().getAndKeyword_1_1()); }
	'and'
	{ after(grammarAccess.getAndAccess().getAndKeyword_1_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group_1__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__And__Group_1__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__And__Group_1__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getAndAccess().getRightAssignment_1_2()); }
	(rule__And__RightAssignment_1_2)
	{ after(grammarAccess.getAndAccess().getRightAssignment_1_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Not__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Not__Group__0__Impl
	rule__Not__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Not__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getNotAccess().getTildeKeyword_0()); }
	'~'
	{ after(grammarAccess.getNotAccess().getTildeKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Not__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Not__Group__1__Impl
	rule__Not__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Not__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); }
	rulePrimary
	{ after(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Not__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Not__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Not__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getNotAccess().getNotRightAction_2()); }
	()
	{ after(grammarAccess.getNotAccess().getNotRightAction_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__PFormula__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__PFormula__Group__0__Impl
	rule__PFormula__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0()); }
	'('
	{ after(grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__PFormula__Group__1__Impl
	rule__PFormula__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPFormulaAccess().getBiimpliesParserRuleCall_1()); }
	ruleBiimplies
	{ after(grammarAccess.getPFormulaAccess().getBiimpliesParserRuleCall_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__PFormula__Group__2__Impl
	rule__PFormula__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPFormulaAccess().getPFormulaExprAction_2()); }
	()
	{ after(grammarAccess.getPFormulaAccess().getPFormulaExprAction_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__PFormula__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__PFormula__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3()); }
	')'
	{ after(grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Start__ChecksAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStartAccess().getChecksCheckParserRuleCall_2_0()); }
		ruleCheck
		{ after(grammarAccess.getStartAccess().getChecksCheckParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__FormulaAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0()); }
		ruleBiimplies
		{ after(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Check__SolverAssignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0()); }
		ruleSatSolver
		{ after(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SatSolver__NameAssignment_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); }
		(
			{ before(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); }
			'SAT4J'
			{ after(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); }
		)
		{ after(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SatSolver__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); }
		(
			{ before(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); }
			'minisat'
			{ after(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); }
		)
		{ after(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__SatSolver__NameAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); }
		(
			{ before(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); }
			'picosat'
			{ after(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); }
		)
		{ after(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Biimplies__RightAssignment_1_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); }
		ruleImplies
		{ after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Implies__RightAssignment_1_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0()); }
		ruleOr
		{ after(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Or__RightAssignment_1_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0()); }
		ruleExcludes
		{ after(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Excludes__RightAssignment_1_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0()); }
		ruleAnd
		{ after(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__And__RightAssignment_1_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getAndAccess().getRightAlternatives_1_2_0()); }
		(rule__And__RightAlternatives_1_2_0)
		{ after(grammarAccess.getAndAccess().getRightAlternatives_1_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Variable__IdAssignment
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); }
		RULE_ID
		{ after(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
