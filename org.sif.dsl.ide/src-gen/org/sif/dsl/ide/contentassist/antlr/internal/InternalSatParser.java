package org.sif.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.sif.dsl.services.SatGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSatParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Check'", "'using'", "';'", "'<=>'", "'=>'", "'or'", "'nand'", "'and'", "'~'", "'('", "')'", "'SAT4J'", "'minisat'", "'picosat'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSatParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSatParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSatParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSat.g"; }


    	private SatGrammarAccess grammarAccess;

    	public void setGrammarAccess(SatGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStart"
    // InternalSat.g:53:1: entryRuleStart : ruleStart EOF ;
    public final void entryRuleStart() throws RecognitionException {
        try {
            // InternalSat.g:54:1: ( ruleStart EOF )
            // InternalSat.g:55:1: ruleStart EOF
            {
             before(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            ruleStart();

            state._fsp--;

             after(grammarAccess.getStartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalSat.g:62:1: ruleStart : ( ( rule__Start__Group__0 ) ) ;
    public final void ruleStart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:66:2: ( ( ( rule__Start__Group__0 ) ) )
            // InternalSat.g:67:2: ( ( rule__Start__Group__0 ) )
            {
            // InternalSat.g:67:2: ( ( rule__Start__Group__0 ) )
            // InternalSat.g:68:3: ( rule__Start__Group__0 )
            {
             before(grammarAccess.getStartAccess().getGroup()); 
            // InternalSat.g:69:3: ( rule__Start__Group__0 )
            // InternalSat.g:69:4: rule__Start__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStartAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleCheck"
    // InternalSat.g:78:1: entryRuleCheck : ruleCheck EOF ;
    public final void entryRuleCheck() throws RecognitionException {
        try {
            // InternalSat.g:79:1: ( ruleCheck EOF )
            // InternalSat.g:80:1: ruleCheck EOF
            {
             before(grammarAccess.getCheckRule()); 
            pushFollow(FOLLOW_1);
            ruleCheck();

            state._fsp--;

             after(grammarAccess.getCheckRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // InternalSat.g:87:1: ruleCheck : ( ( rule__Check__Group__0 ) ) ;
    public final void ruleCheck() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:91:2: ( ( ( rule__Check__Group__0 ) ) )
            // InternalSat.g:92:2: ( ( rule__Check__Group__0 ) )
            {
            // InternalSat.g:92:2: ( ( rule__Check__Group__0 ) )
            // InternalSat.g:93:3: ( rule__Check__Group__0 )
            {
             before(grammarAccess.getCheckAccess().getGroup()); 
            // InternalSat.g:94:3: ( rule__Check__Group__0 )
            // InternalSat.g:94:4: rule__Check__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Check__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleSatSolver"
    // InternalSat.g:103:1: entryRuleSatSolver : ruleSatSolver EOF ;
    public final void entryRuleSatSolver() throws RecognitionException {
        try {
            // InternalSat.g:104:1: ( ruleSatSolver EOF )
            // InternalSat.g:105:1: ruleSatSolver EOF
            {
             before(grammarAccess.getSatSolverRule()); 
            pushFollow(FOLLOW_1);
            ruleSatSolver();

            state._fsp--;

             after(grammarAccess.getSatSolverRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSatSolver"


    // $ANTLR start "ruleSatSolver"
    // InternalSat.g:112:1: ruleSatSolver : ( ( rule__SatSolver__Alternatives ) ) ;
    public final void ruleSatSolver() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:116:2: ( ( ( rule__SatSolver__Alternatives ) ) )
            // InternalSat.g:117:2: ( ( rule__SatSolver__Alternatives ) )
            {
            // InternalSat.g:117:2: ( ( rule__SatSolver__Alternatives ) )
            // InternalSat.g:118:3: ( rule__SatSolver__Alternatives )
            {
             before(grammarAccess.getSatSolverAccess().getAlternatives()); 
            // InternalSat.g:119:3: ( rule__SatSolver__Alternatives )
            // InternalSat.g:119:4: rule__SatSolver__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SatSolver__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSatSolverAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSatSolver"


    // $ANTLR start "entryRuleBiimplies"
    // InternalSat.g:128:1: entryRuleBiimplies : ruleBiimplies EOF ;
    public final void entryRuleBiimplies() throws RecognitionException {
        try {
            // InternalSat.g:129:1: ( ruleBiimplies EOF )
            // InternalSat.g:130:1: ruleBiimplies EOF
            {
             before(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalSat.g:137:1: ruleBiimplies : ( ( rule__Biimplies__Group__0 ) ) ;
    public final void ruleBiimplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:141:2: ( ( ( rule__Biimplies__Group__0 ) ) )
            // InternalSat.g:142:2: ( ( rule__Biimplies__Group__0 ) )
            {
            // InternalSat.g:142:2: ( ( rule__Biimplies__Group__0 ) )
            // InternalSat.g:143:3: ( rule__Biimplies__Group__0 )
            {
             before(grammarAccess.getBiimpliesAccess().getGroup()); 
            // InternalSat.g:144:3: ( rule__Biimplies__Group__0 )
            // InternalSat.g:144:4: rule__Biimplies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalSat.g:153:1: entryRuleImplies : ruleImplies EOF ;
    public final void entryRuleImplies() throws RecognitionException {
        try {
            // InternalSat.g:154:1: ( ruleImplies EOF )
            // InternalSat.g:155:1: ruleImplies EOF
            {
             before(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getImpliesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalSat.g:162:1: ruleImplies : ( ( rule__Implies__Group__0 ) ) ;
    public final void ruleImplies() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:166:2: ( ( ( rule__Implies__Group__0 ) ) )
            // InternalSat.g:167:2: ( ( rule__Implies__Group__0 ) )
            {
            // InternalSat.g:167:2: ( ( rule__Implies__Group__0 ) )
            // InternalSat.g:168:3: ( rule__Implies__Group__0 )
            {
             before(grammarAccess.getImpliesAccess().getGroup()); 
            // InternalSat.g:169:3: ( rule__Implies__Group__0 )
            // InternalSat.g:169:4: rule__Implies__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleOr"
    // InternalSat.g:178:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // InternalSat.g:179:1: ( ruleOr EOF )
            // InternalSat.g:180:1: ruleOr EOF
            {
             before(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getOrRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalSat.g:187:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:191:2: ( ( ( rule__Or__Group__0 ) ) )
            // InternalSat.g:192:2: ( ( rule__Or__Group__0 ) )
            {
            // InternalSat.g:192:2: ( ( rule__Or__Group__0 ) )
            // InternalSat.g:193:3: ( rule__Or__Group__0 )
            {
             before(grammarAccess.getOrAccess().getGroup()); 
            // InternalSat.g:194:3: ( rule__Or__Group__0 )
            // InternalSat.g:194:4: rule__Or__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleExcludes"
    // InternalSat.g:203:1: entryRuleExcludes : ruleExcludes EOF ;
    public final void entryRuleExcludes() throws RecognitionException {
        try {
            // InternalSat.g:204:1: ( ruleExcludes EOF )
            // InternalSat.g:205:1: ruleExcludes EOF
            {
             before(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getExcludesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalSat.g:212:1: ruleExcludes : ( ( rule__Excludes__Group__0 ) ) ;
    public final void ruleExcludes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:216:2: ( ( ( rule__Excludes__Group__0 ) ) )
            // InternalSat.g:217:2: ( ( rule__Excludes__Group__0 ) )
            {
            // InternalSat.g:217:2: ( ( rule__Excludes__Group__0 ) )
            // InternalSat.g:218:3: ( rule__Excludes__Group__0 )
            {
             before(grammarAccess.getExcludesAccess().getGroup()); 
            // InternalSat.g:219:3: ( rule__Excludes__Group__0 )
            // InternalSat.g:219:4: rule__Excludes__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleAnd"
    // InternalSat.g:228:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // InternalSat.g:229:1: ( ruleAnd EOF )
            // InternalSat.g:230:1: ruleAnd EOF
            {
             before(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getAndRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalSat.g:237:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:241:2: ( ( ( rule__And__Group__0 ) ) )
            // InternalSat.g:242:2: ( ( rule__And__Group__0 ) )
            {
            // InternalSat.g:242:2: ( ( rule__And__Group__0 ) )
            // InternalSat.g:243:3: ( rule__And__Group__0 )
            {
             before(grammarAccess.getAndAccess().getGroup()); 
            // InternalSat.g:244:3: ( rule__And__Group__0 )
            // InternalSat.g:244:4: rule__And__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalSat.g:253:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalSat.g:254:1: ( ruleNot EOF )
            // InternalSat.g:255:1: ruleNot EOF
            {
             before(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;

             after(grammarAccess.getNotRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalSat.g:262:1: ruleNot : ( ( rule__Not__Group__0 ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:266:2: ( ( ( rule__Not__Group__0 ) ) )
            // InternalSat.g:267:2: ( ( rule__Not__Group__0 ) )
            {
            // InternalSat.g:267:2: ( ( rule__Not__Group__0 ) )
            // InternalSat.g:268:3: ( rule__Not__Group__0 )
            {
             before(grammarAccess.getNotAccess().getGroup()); 
            // InternalSat.g:269:3: ( rule__Not__Group__0 )
            // InternalSat.g:269:4: rule__Not__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalSat.g:278:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalSat.g:279:1: ( rulePrimary EOF )
            // InternalSat.g:280:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalSat.g:287:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:291:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalSat.g:292:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalSat.g:292:2: ( ( rule__Primary__Alternatives ) )
            // InternalSat.g:293:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalSat.g:294:3: ( rule__Primary__Alternatives )
            // InternalSat.g:294:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleVariable"
    // InternalSat.g:303:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalSat.g:304:1: ( ruleVariable EOF )
            // InternalSat.g:305:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalSat.g:312:1: ruleVariable : ( ( rule__Variable__IdAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:316:2: ( ( ( rule__Variable__IdAssignment ) ) )
            // InternalSat.g:317:2: ( ( rule__Variable__IdAssignment ) )
            {
            // InternalSat.g:317:2: ( ( rule__Variable__IdAssignment ) )
            // InternalSat.g:318:3: ( rule__Variable__IdAssignment )
            {
             before(grammarAccess.getVariableAccess().getIdAssignment()); 
            // InternalSat.g:319:3: ( rule__Variable__IdAssignment )
            // InternalSat.g:319:4: rule__Variable__IdAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Variable__IdAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getIdAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRulePFormula"
    // InternalSat.g:328:1: entryRulePFormula : rulePFormula EOF ;
    public final void entryRulePFormula() throws RecognitionException {
        try {
            // InternalSat.g:329:1: ( rulePFormula EOF )
            // InternalSat.g:330:1: rulePFormula EOF
            {
             before(grammarAccess.getPFormulaRule()); 
            pushFollow(FOLLOW_1);
            rulePFormula();

            state._fsp--;

             after(grammarAccess.getPFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePFormula"


    // $ANTLR start "rulePFormula"
    // InternalSat.g:337:1: rulePFormula : ( ( rule__PFormula__Group__0 ) ) ;
    public final void rulePFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:341:2: ( ( ( rule__PFormula__Group__0 ) ) )
            // InternalSat.g:342:2: ( ( rule__PFormula__Group__0 ) )
            {
            // InternalSat.g:342:2: ( ( rule__PFormula__Group__0 ) )
            // InternalSat.g:343:3: ( rule__PFormula__Group__0 )
            {
             before(grammarAccess.getPFormulaAccess().getGroup()); 
            // InternalSat.g:344:3: ( rule__PFormula__Group__0 )
            // InternalSat.g:344:4: rule__PFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePFormula"


    // $ANTLR start "rule__SatSolver__Alternatives"
    // InternalSat.g:352:1: rule__SatSolver__Alternatives : ( ( ( rule__SatSolver__NameAssignment_0 ) ) | ( ( rule__SatSolver__NameAssignment_1 ) ) | ( ( rule__SatSolver__NameAssignment_2 ) ) );
    public final void rule__SatSolver__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:356:1: ( ( ( rule__SatSolver__NameAssignment_0 ) ) | ( ( rule__SatSolver__NameAssignment_1 ) ) | ( ( rule__SatSolver__NameAssignment_2 ) ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt1=1;
                }
                break;
            case 23:
                {
                alt1=2;
                }
                break;
            case 24:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalSat.g:357:2: ( ( rule__SatSolver__NameAssignment_0 ) )
                    {
                    // InternalSat.g:357:2: ( ( rule__SatSolver__NameAssignment_0 ) )
                    // InternalSat.g:358:3: ( rule__SatSolver__NameAssignment_0 )
                    {
                     before(grammarAccess.getSatSolverAccess().getNameAssignment_0()); 
                    // InternalSat.g:359:3: ( rule__SatSolver__NameAssignment_0 )
                    // InternalSat.g:359:4: rule__SatSolver__NameAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SatSolver__NameAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSatSolverAccess().getNameAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSat.g:363:2: ( ( rule__SatSolver__NameAssignment_1 ) )
                    {
                    // InternalSat.g:363:2: ( ( rule__SatSolver__NameAssignment_1 ) )
                    // InternalSat.g:364:3: ( rule__SatSolver__NameAssignment_1 )
                    {
                     before(grammarAccess.getSatSolverAccess().getNameAssignment_1()); 
                    // InternalSat.g:365:3: ( rule__SatSolver__NameAssignment_1 )
                    // InternalSat.g:365:4: rule__SatSolver__NameAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SatSolver__NameAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getSatSolverAccess().getNameAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSat.g:369:2: ( ( rule__SatSolver__NameAssignment_2 ) )
                    {
                    // InternalSat.g:369:2: ( ( rule__SatSolver__NameAssignment_2 ) )
                    // InternalSat.g:370:3: ( rule__SatSolver__NameAssignment_2 )
                    {
                     before(grammarAccess.getSatSolverAccess().getNameAssignment_2()); 
                    // InternalSat.g:371:3: ( rule__SatSolver__NameAssignment_2 )
                    // InternalSat.g:371:4: rule__SatSolver__NameAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SatSolver__NameAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getSatSolverAccess().getNameAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SatSolver__Alternatives"


    // $ANTLR start "rule__And__Alternatives_0"
    // InternalSat.g:379:1: rule__And__Alternatives_0 : ( ( ruleNot ) | ( rulePrimary ) );
    public final void rule__And__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:383:1: ( ( ruleNot ) | ( rulePrimary ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==19) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID||LA2_0==20) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalSat.g:384:2: ( ruleNot )
                    {
                    // InternalSat.g:384:2: ( ruleNot )
                    // InternalSat.g:385:3: ruleNot
                    {
                     before(grammarAccess.getAndAccess().getNotParserRuleCall_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleNot();

                    state._fsp--;

                     after(grammarAccess.getAndAccess().getNotParserRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSat.g:390:2: ( rulePrimary )
                    {
                    // InternalSat.g:390:2: ( rulePrimary )
                    // InternalSat.g:391:3: rulePrimary
                    {
                     before(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary();

                    state._fsp--;

                     after(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Alternatives_0"


    // $ANTLR start "rule__And__RightAlternatives_1_2_0"
    // InternalSat.g:400:1: rule__And__RightAlternatives_1_2_0 : ( ( ruleNot ) | ( rulePrimary ) );
    public final void rule__And__RightAlternatives_1_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:404:1: ( ( ruleNot ) | ( rulePrimary ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==19) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID||LA3_0==20) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalSat.g:405:2: ( ruleNot )
                    {
                    // InternalSat.g:405:2: ( ruleNot )
                    // InternalSat.g:406:3: ruleNot
                    {
                     before(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleNot();

                    state._fsp--;

                     after(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSat.g:411:2: ( rulePrimary )
                    {
                    // InternalSat.g:411:2: ( rulePrimary )
                    // InternalSat.g:412:3: rulePrimary
                    {
                     before(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0_1()); 
                    pushFollow(FOLLOW_2);
                    rulePrimary();

                    state._fsp--;

                     after(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAlternatives_1_2_0"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalSat.g:421:1: rule__Primary__Alternatives : ( ( ruleVariable ) | ( rulePFormula ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:425:1: ( ( ruleVariable ) | ( rulePFormula ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==20) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalSat.g:426:2: ( ruleVariable )
                    {
                    // InternalSat.g:426:2: ( ruleVariable )
                    // InternalSat.g:427:3: ruleVariable
                    {
                     before(grammarAccess.getPrimaryAccess().getVariableParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getVariableParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSat.g:432:2: ( rulePFormula )
                    {
                    // InternalSat.g:432:2: ( rulePFormula )
                    // InternalSat.g:433:3: rulePFormula
                    {
                     before(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePFormula();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Start__Group__0"
    // InternalSat.g:442:1: rule__Start__Group__0 : rule__Start__Group__0__Impl rule__Start__Group__1 ;
    public final void rule__Start__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:446:1: ( rule__Start__Group__0__Impl rule__Start__Group__1 )
            // InternalSat.g:447:2: rule__Start__Group__0__Impl rule__Start__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Start__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__0"


    // $ANTLR start "rule__Start__Group__0__Impl"
    // InternalSat.g:454:1: rule__Start__Group__0__Impl : ( ruleCheck ) ;
    public final void rule__Start__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:458:1: ( ( ruleCheck ) )
            // InternalSat.g:459:1: ( ruleCheck )
            {
            // InternalSat.g:459:1: ( ruleCheck )
            // InternalSat.g:460:2: ruleCheck
            {
             before(grammarAccess.getStartAccess().getCheckParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleCheck();

            state._fsp--;

             after(grammarAccess.getStartAccess().getCheckParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__0__Impl"


    // $ANTLR start "rule__Start__Group__1"
    // InternalSat.g:469:1: rule__Start__Group__1 : rule__Start__Group__1__Impl rule__Start__Group__2 ;
    public final void rule__Start__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:473:1: ( rule__Start__Group__1__Impl rule__Start__Group__2 )
            // InternalSat.g:474:2: rule__Start__Group__1__Impl rule__Start__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Start__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Start__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__1"


    // $ANTLR start "rule__Start__Group__1__Impl"
    // InternalSat.g:481:1: rule__Start__Group__1__Impl : ( () ) ;
    public final void rule__Start__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:485:1: ( ( () ) )
            // InternalSat.g:486:1: ( () )
            {
            // InternalSat.g:486:1: ( () )
            // InternalSat.g:487:2: ()
            {
             before(grammarAccess.getStartAccess().getStartChecksAction_1()); 
            // InternalSat.g:488:2: ()
            // InternalSat.g:488:3: 
            {
            }

             after(grammarAccess.getStartAccess().getStartChecksAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__1__Impl"


    // $ANTLR start "rule__Start__Group__2"
    // InternalSat.g:496:1: rule__Start__Group__2 : rule__Start__Group__2__Impl ;
    public final void rule__Start__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:500:1: ( rule__Start__Group__2__Impl )
            // InternalSat.g:501:2: rule__Start__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Start__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__2"


    // $ANTLR start "rule__Start__Group__2__Impl"
    // InternalSat.g:507:1: rule__Start__Group__2__Impl : ( ( rule__Start__ChecksAssignment_2 )* ) ;
    public final void rule__Start__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:511:1: ( ( ( rule__Start__ChecksAssignment_2 )* ) )
            // InternalSat.g:512:1: ( ( rule__Start__ChecksAssignment_2 )* )
            {
            // InternalSat.g:512:1: ( ( rule__Start__ChecksAssignment_2 )* )
            // InternalSat.g:513:2: ( rule__Start__ChecksAssignment_2 )*
            {
             before(grammarAccess.getStartAccess().getChecksAssignment_2()); 
            // InternalSat.g:514:2: ( rule__Start__ChecksAssignment_2 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==11) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSat.g:514:3: rule__Start__ChecksAssignment_2
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Start__ChecksAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getStartAccess().getChecksAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__Group__2__Impl"


    // $ANTLR start "rule__Check__Group__0"
    // InternalSat.g:523:1: rule__Check__Group__0 : rule__Check__Group__0__Impl rule__Check__Group__1 ;
    public final void rule__Check__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:527:1: ( rule__Check__Group__0__Impl rule__Check__Group__1 )
            // InternalSat.g:528:2: rule__Check__Group__0__Impl rule__Check__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Check__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__0"


    // $ANTLR start "rule__Check__Group__0__Impl"
    // InternalSat.g:535:1: rule__Check__Group__0__Impl : ( 'Check' ) ;
    public final void rule__Check__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:539:1: ( ( 'Check' ) )
            // InternalSat.g:540:1: ( 'Check' )
            {
            // InternalSat.g:540:1: ( 'Check' )
            // InternalSat.g:541:2: 'Check'
            {
             before(grammarAccess.getCheckAccess().getCheckKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getCheckAccess().getCheckKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__0__Impl"


    // $ANTLR start "rule__Check__Group__1"
    // InternalSat.g:550:1: rule__Check__Group__1 : rule__Check__Group__1__Impl rule__Check__Group__2 ;
    public final void rule__Check__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:554:1: ( rule__Check__Group__1__Impl rule__Check__Group__2 )
            // InternalSat.g:555:2: rule__Check__Group__1__Impl rule__Check__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Check__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__1"


    // $ANTLR start "rule__Check__Group__1__Impl"
    // InternalSat.g:562:1: rule__Check__Group__1__Impl : ( ( rule__Check__FormulaAssignment_1 ) ) ;
    public final void rule__Check__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:566:1: ( ( ( rule__Check__FormulaAssignment_1 ) ) )
            // InternalSat.g:567:1: ( ( rule__Check__FormulaAssignment_1 ) )
            {
            // InternalSat.g:567:1: ( ( rule__Check__FormulaAssignment_1 ) )
            // InternalSat.g:568:2: ( rule__Check__FormulaAssignment_1 )
            {
             before(grammarAccess.getCheckAccess().getFormulaAssignment_1()); 
            // InternalSat.g:569:2: ( rule__Check__FormulaAssignment_1 )
            // InternalSat.g:569:3: rule__Check__FormulaAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Check__FormulaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getFormulaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__1__Impl"


    // $ANTLR start "rule__Check__Group__2"
    // InternalSat.g:577:1: rule__Check__Group__2 : rule__Check__Group__2__Impl rule__Check__Group__3 ;
    public final void rule__Check__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:581:1: ( rule__Check__Group__2__Impl rule__Check__Group__3 )
            // InternalSat.g:582:2: rule__Check__Group__2__Impl rule__Check__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Check__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__2"


    // $ANTLR start "rule__Check__Group__2__Impl"
    // InternalSat.g:589:1: rule__Check__Group__2__Impl : ( 'using' ) ;
    public final void rule__Check__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:593:1: ( ( 'using' ) )
            // InternalSat.g:594:1: ( 'using' )
            {
            // InternalSat.g:594:1: ( 'using' )
            // InternalSat.g:595:2: 'using'
            {
             before(grammarAccess.getCheckAccess().getUsingKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getCheckAccess().getUsingKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__2__Impl"


    // $ANTLR start "rule__Check__Group__3"
    // InternalSat.g:604:1: rule__Check__Group__3 : rule__Check__Group__3__Impl rule__Check__Group__4 ;
    public final void rule__Check__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:608:1: ( rule__Check__Group__3__Impl rule__Check__Group__4 )
            // InternalSat.g:609:2: rule__Check__Group__3__Impl rule__Check__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Check__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Check__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__3"


    // $ANTLR start "rule__Check__Group__3__Impl"
    // InternalSat.g:616:1: rule__Check__Group__3__Impl : ( ( rule__Check__SolverAssignment_3 ) ) ;
    public final void rule__Check__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:620:1: ( ( ( rule__Check__SolverAssignment_3 ) ) )
            // InternalSat.g:621:1: ( ( rule__Check__SolverAssignment_3 ) )
            {
            // InternalSat.g:621:1: ( ( rule__Check__SolverAssignment_3 ) )
            // InternalSat.g:622:2: ( rule__Check__SolverAssignment_3 )
            {
             before(grammarAccess.getCheckAccess().getSolverAssignment_3()); 
            // InternalSat.g:623:2: ( rule__Check__SolverAssignment_3 )
            // InternalSat.g:623:3: rule__Check__SolverAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Check__SolverAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCheckAccess().getSolverAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__3__Impl"


    // $ANTLR start "rule__Check__Group__4"
    // InternalSat.g:631:1: rule__Check__Group__4 : rule__Check__Group__4__Impl ;
    public final void rule__Check__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:635:1: ( rule__Check__Group__4__Impl )
            // InternalSat.g:636:2: rule__Check__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Check__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__4"


    // $ANTLR start "rule__Check__Group__4__Impl"
    // InternalSat.g:642:1: rule__Check__Group__4__Impl : ( ';' ) ;
    public final void rule__Check__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:646:1: ( ( ';' ) )
            // InternalSat.g:647:1: ( ';' )
            {
            // InternalSat.g:647:1: ( ';' )
            // InternalSat.g:648:2: ';'
            {
             before(grammarAccess.getCheckAccess().getSemicolonKeyword_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getCheckAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__4__Impl"


    // $ANTLR start "rule__Biimplies__Group__0"
    // InternalSat.g:658:1: rule__Biimplies__Group__0 : rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 ;
    public final void rule__Biimplies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:662:1: ( rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1 )
            // InternalSat.g:663:2: rule__Biimplies__Group__0__Impl rule__Biimplies__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Biimplies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0"


    // $ANTLR start "rule__Biimplies__Group__0__Impl"
    // InternalSat.g:670:1: rule__Biimplies__Group__0__Impl : ( ruleImplies ) ;
    public final void rule__Biimplies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:674:1: ( ( ruleImplies ) )
            // InternalSat.g:675:1: ( ruleImplies )
            {
            // InternalSat.g:675:1: ( ruleImplies )
            // InternalSat.g:676:2: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__0__Impl"


    // $ANTLR start "rule__Biimplies__Group__1"
    // InternalSat.g:685:1: rule__Biimplies__Group__1 : rule__Biimplies__Group__1__Impl ;
    public final void rule__Biimplies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:689:1: ( rule__Biimplies__Group__1__Impl )
            // InternalSat.g:690:2: rule__Biimplies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1"


    // $ANTLR start "rule__Biimplies__Group__1__Impl"
    // InternalSat.g:696:1: rule__Biimplies__Group__1__Impl : ( ( rule__Biimplies__Group_1__0 )* ) ;
    public final void rule__Biimplies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:700:1: ( ( ( rule__Biimplies__Group_1__0 )* ) )
            // InternalSat.g:701:1: ( ( rule__Biimplies__Group_1__0 )* )
            {
            // InternalSat.g:701:1: ( ( rule__Biimplies__Group_1__0 )* )
            // InternalSat.g:702:2: ( rule__Biimplies__Group_1__0 )*
            {
             before(grammarAccess.getBiimpliesAccess().getGroup_1()); 
            // InternalSat.g:703:2: ( rule__Biimplies__Group_1__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==14) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalSat.g:703:3: rule__Biimplies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Biimplies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getBiimpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__0"
    // InternalSat.g:712:1: rule__Biimplies__Group_1__0 : rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 ;
    public final void rule__Biimplies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:716:1: ( rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1 )
            // InternalSat.g:717:2: rule__Biimplies__Group_1__0__Impl rule__Biimplies__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__Biimplies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0"


    // $ANTLR start "rule__Biimplies__Group_1__0__Impl"
    // InternalSat.g:724:1: rule__Biimplies__Group_1__0__Impl : ( () ) ;
    public final void rule__Biimplies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:728:1: ( ( () ) )
            // InternalSat.g:729:1: ( () )
            {
            // InternalSat.g:729:1: ( () )
            // InternalSat.g:730:2: ()
            {
             before(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 
            // InternalSat.g:731:2: ()
            // InternalSat.g:731:3: 
            {
            }

             after(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__0__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__1"
    // InternalSat.g:739:1: rule__Biimplies__Group_1__1 : rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 ;
    public final void rule__Biimplies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:743:1: ( rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2 )
            // InternalSat.g:744:2: rule__Biimplies__Group_1__1__Impl rule__Biimplies__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Biimplies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1"


    // $ANTLR start "rule__Biimplies__Group_1__1__Impl"
    // InternalSat.g:751:1: rule__Biimplies__Group_1__1__Impl : ( '<=>' ) ;
    public final void rule__Biimplies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:755:1: ( ( '<=>' ) )
            // InternalSat.g:756:1: ( '<=>' )
            {
            // InternalSat.g:756:1: ( '<=>' )
            // InternalSat.g:757:2: '<=>'
            {
             before(grammarAccess.getBiimpliesAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getBiimpliesAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__1__Impl"


    // $ANTLR start "rule__Biimplies__Group_1__2"
    // InternalSat.g:766:1: rule__Biimplies__Group_1__2 : rule__Biimplies__Group_1__2__Impl ;
    public final void rule__Biimplies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:770:1: ( rule__Biimplies__Group_1__2__Impl )
            // InternalSat.g:771:2: rule__Biimplies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2"


    // $ANTLR start "rule__Biimplies__Group_1__2__Impl"
    // InternalSat.g:777:1: rule__Biimplies__Group_1__2__Impl : ( ( rule__Biimplies__RightAssignment_1_2 ) ) ;
    public final void rule__Biimplies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:781:1: ( ( ( rule__Biimplies__RightAssignment_1_2 ) ) )
            // InternalSat.g:782:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            {
            // InternalSat.g:782:1: ( ( rule__Biimplies__RightAssignment_1_2 ) )
            // InternalSat.g:783:2: ( rule__Biimplies__RightAssignment_1_2 )
            {
             before(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 
            // InternalSat.g:784:2: ( rule__Biimplies__RightAssignment_1_2 )
            // InternalSat.g:784:3: rule__Biimplies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Biimplies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getBiimpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__Group_1__2__Impl"


    // $ANTLR start "rule__Implies__Group__0"
    // InternalSat.g:793:1: rule__Implies__Group__0 : rule__Implies__Group__0__Impl rule__Implies__Group__1 ;
    public final void rule__Implies__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:797:1: ( rule__Implies__Group__0__Impl rule__Implies__Group__1 )
            // InternalSat.g:798:2: rule__Implies__Group__0__Impl rule__Implies__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0"


    // $ANTLR start "rule__Implies__Group__0__Impl"
    // InternalSat.g:805:1: rule__Implies__Group__0__Impl : ( ruleOr ) ;
    public final void rule__Implies__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:809:1: ( ( ruleOr ) )
            // InternalSat.g:810:1: ( ruleOr )
            {
            // InternalSat.g:810:1: ( ruleOr )
            // InternalSat.g:811:2: ruleOr
            {
             before(grammarAccess.getImpliesAccess().getOrParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getOrParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__0__Impl"


    // $ANTLR start "rule__Implies__Group__1"
    // InternalSat.g:820:1: rule__Implies__Group__1 : rule__Implies__Group__1__Impl ;
    public final void rule__Implies__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:824:1: ( rule__Implies__Group__1__Impl )
            // InternalSat.g:825:2: rule__Implies__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1"


    // $ANTLR start "rule__Implies__Group__1__Impl"
    // InternalSat.g:831:1: rule__Implies__Group__1__Impl : ( ( rule__Implies__Group_1__0 )* ) ;
    public final void rule__Implies__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:835:1: ( ( ( rule__Implies__Group_1__0 )* ) )
            // InternalSat.g:836:1: ( ( rule__Implies__Group_1__0 )* )
            {
            // InternalSat.g:836:1: ( ( rule__Implies__Group_1__0 )* )
            // InternalSat.g:837:2: ( rule__Implies__Group_1__0 )*
            {
             before(grammarAccess.getImpliesAccess().getGroup_1()); 
            // InternalSat.g:838:2: ( rule__Implies__Group_1__0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==15) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalSat.g:838:3: rule__Implies__Group_1__0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Implies__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getImpliesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__0"
    // InternalSat.g:847:1: rule__Implies__Group_1__0 : rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 ;
    public final void rule__Implies__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:851:1: ( rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1 )
            // InternalSat.g:852:2: rule__Implies__Group_1__0__Impl rule__Implies__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Implies__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0"


    // $ANTLR start "rule__Implies__Group_1__0__Impl"
    // InternalSat.g:859:1: rule__Implies__Group_1__0__Impl : ( () ) ;
    public final void rule__Implies__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:863:1: ( ( () ) )
            // InternalSat.g:864:1: ( () )
            {
            // InternalSat.g:864:1: ( () )
            // InternalSat.g:865:2: ()
            {
             before(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 
            // InternalSat.g:866:2: ()
            // InternalSat.g:866:3: 
            {
            }

             after(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__0__Impl"


    // $ANTLR start "rule__Implies__Group_1__1"
    // InternalSat.g:874:1: rule__Implies__Group_1__1 : rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 ;
    public final void rule__Implies__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:878:1: ( rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2 )
            // InternalSat.g:879:2: rule__Implies__Group_1__1__Impl rule__Implies__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Implies__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1"


    // $ANTLR start "rule__Implies__Group_1__1__Impl"
    // InternalSat.g:886:1: rule__Implies__Group_1__1__Impl : ( '=>' ) ;
    public final void rule__Implies__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:890:1: ( ( '=>' ) )
            // InternalSat.g:891:1: ( '=>' )
            {
            // InternalSat.g:891:1: ( '=>' )
            // InternalSat.g:892:2: '=>'
            {
             before(grammarAccess.getImpliesAccess().getEqualsSignGreaterThanSignKeyword_1_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getImpliesAccess().getEqualsSignGreaterThanSignKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__1__Impl"


    // $ANTLR start "rule__Implies__Group_1__2"
    // InternalSat.g:901:1: rule__Implies__Group_1__2 : rule__Implies__Group_1__2__Impl ;
    public final void rule__Implies__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:905:1: ( rule__Implies__Group_1__2__Impl )
            // InternalSat.g:906:2: rule__Implies__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Implies__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2"


    // $ANTLR start "rule__Implies__Group_1__2__Impl"
    // InternalSat.g:912:1: rule__Implies__Group_1__2__Impl : ( ( rule__Implies__RightAssignment_1_2 ) ) ;
    public final void rule__Implies__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:916:1: ( ( ( rule__Implies__RightAssignment_1_2 ) ) )
            // InternalSat.g:917:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            {
            // InternalSat.g:917:1: ( ( rule__Implies__RightAssignment_1_2 ) )
            // InternalSat.g:918:2: ( rule__Implies__RightAssignment_1_2 )
            {
             before(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 
            // InternalSat.g:919:2: ( rule__Implies__RightAssignment_1_2 )
            // InternalSat.g:919:3: rule__Implies__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Implies__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getImpliesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__Group_1__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // InternalSat.g:928:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:932:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // InternalSat.g:933:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Or__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // InternalSat.g:940:1: rule__Or__Group__0__Impl : ( ruleExcludes ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:944:1: ( ( ruleExcludes ) )
            // InternalSat.g:945:1: ( ruleExcludes )
            {
            // InternalSat.g:945:1: ( ruleExcludes )
            // InternalSat.g:946:2: ruleExcludes
            {
             before(grammarAccess.getOrAccess().getExcludesParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getOrAccess().getExcludesParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // InternalSat.g:955:1: rule__Or__Group__1 : rule__Or__Group__1__Impl ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:959:1: ( rule__Or__Group__1__Impl )
            // InternalSat.g:960:2: rule__Or__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // InternalSat.g:966:1: rule__Or__Group__1__Impl : ( ( rule__Or__Group_1__0 )* ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:970:1: ( ( ( rule__Or__Group_1__0 )* ) )
            // InternalSat.g:971:1: ( ( rule__Or__Group_1__0 )* )
            {
            // InternalSat.g:971:1: ( ( rule__Or__Group_1__0 )* )
            // InternalSat.g:972:2: ( rule__Or__Group_1__0 )*
            {
             before(grammarAccess.getOrAccess().getGroup_1()); 
            // InternalSat.g:973:2: ( rule__Or__Group_1__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==16) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalSat.g:973:3: rule__Or__Group_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Or__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getOrAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group_1__0"
    // InternalSat.g:982:1: rule__Or__Group_1__0 : rule__Or__Group_1__0__Impl rule__Or__Group_1__1 ;
    public final void rule__Or__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:986:1: ( rule__Or__Group_1__0__Impl rule__Or__Group_1__1 )
            // InternalSat.g:987:2: rule__Or__Group_1__0__Impl rule__Or__Group_1__1
            {
            pushFollow(FOLLOW_13);
            rule__Or__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0"


    // $ANTLR start "rule__Or__Group_1__0__Impl"
    // InternalSat.g:994:1: rule__Or__Group_1__0__Impl : ( () ) ;
    public final void rule__Or__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:998:1: ( ( () ) )
            // InternalSat.g:999:1: ( () )
            {
            // InternalSat.g:999:1: ( () )
            // InternalSat.g:1000:2: ()
            {
             before(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 
            // InternalSat.g:1001:2: ()
            // InternalSat.g:1001:3: 
            {
            }

             after(grammarAccess.getOrAccess().getOrLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__0__Impl"


    // $ANTLR start "rule__Or__Group_1__1"
    // InternalSat.g:1009:1: rule__Or__Group_1__1 : rule__Or__Group_1__1__Impl rule__Or__Group_1__2 ;
    public final void rule__Or__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1013:1: ( rule__Or__Group_1__1__Impl rule__Or__Group_1__2 )
            // InternalSat.g:1014:2: rule__Or__Group_1__1__Impl rule__Or__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Or__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1"


    // $ANTLR start "rule__Or__Group_1__1__Impl"
    // InternalSat.g:1021:1: rule__Or__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__Or__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1025:1: ( ( 'or' ) )
            // InternalSat.g:1026:1: ( 'or' )
            {
            // InternalSat.g:1026:1: ( 'or' )
            // InternalSat.g:1027:2: 'or'
            {
             before(grammarAccess.getOrAccess().getOrKeyword_1_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getOrAccess().getOrKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__1__Impl"


    // $ANTLR start "rule__Or__Group_1__2"
    // InternalSat.g:1036:1: rule__Or__Group_1__2 : rule__Or__Group_1__2__Impl ;
    public final void rule__Or__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1040:1: ( rule__Or__Group_1__2__Impl )
            // InternalSat.g:1041:2: rule__Or__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Or__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2"


    // $ANTLR start "rule__Or__Group_1__2__Impl"
    // InternalSat.g:1047:1: rule__Or__Group_1__2__Impl : ( ( rule__Or__RightAssignment_1_2 ) ) ;
    public final void rule__Or__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1051:1: ( ( ( rule__Or__RightAssignment_1_2 ) ) )
            // InternalSat.g:1052:1: ( ( rule__Or__RightAssignment_1_2 ) )
            {
            // InternalSat.g:1052:1: ( ( rule__Or__RightAssignment_1_2 ) )
            // InternalSat.g:1053:2: ( rule__Or__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrAccess().getRightAssignment_1_2()); 
            // InternalSat.g:1054:2: ( rule__Or__RightAssignment_1_2 )
            // InternalSat.g:1054:3: rule__Or__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Or__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group_1__2__Impl"


    // $ANTLR start "rule__Excludes__Group__0"
    // InternalSat.g:1063:1: rule__Excludes__Group__0 : rule__Excludes__Group__0__Impl rule__Excludes__Group__1 ;
    public final void rule__Excludes__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1067:1: ( rule__Excludes__Group__0__Impl rule__Excludes__Group__1 )
            // InternalSat.g:1068:2: rule__Excludes__Group__0__Impl rule__Excludes__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Excludes__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0"


    // $ANTLR start "rule__Excludes__Group__0__Impl"
    // InternalSat.g:1075:1: rule__Excludes__Group__0__Impl : ( ruleAnd ) ;
    public final void rule__Excludes__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1079:1: ( ( ruleAnd ) )
            // InternalSat.g:1080:1: ( ruleAnd )
            {
            // InternalSat.g:1080:1: ( ruleAnd )
            // InternalSat.g:1081:2: ruleAnd
            {
             before(grammarAccess.getExcludesAccess().getAndParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getAndParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__0__Impl"


    // $ANTLR start "rule__Excludes__Group__1"
    // InternalSat.g:1090:1: rule__Excludes__Group__1 : rule__Excludes__Group__1__Impl ;
    public final void rule__Excludes__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1094:1: ( rule__Excludes__Group__1__Impl )
            // InternalSat.g:1095:2: rule__Excludes__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1"


    // $ANTLR start "rule__Excludes__Group__1__Impl"
    // InternalSat.g:1101:1: rule__Excludes__Group__1__Impl : ( ( rule__Excludes__Group_1__0 )* ) ;
    public final void rule__Excludes__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1105:1: ( ( ( rule__Excludes__Group_1__0 )* ) )
            // InternalSat.g:1106:1: ( ( rule__Excludes__Group_1__0 )* )
            {
            // InternalSat.g:1106:1: ( ( rule__Excludes__Group_1__0 )* )
            // InternalSat.g:1107:2: ( rule__Excludes__Group_1__0 )*
            {
             before(grammarAccess.getExcludesAccess().getGroup_1()); 
            // InternalSat.g:1108:2: ( rule__Excludes__Group_1__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==17) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalSat.g:1108:3: rule__Excludes__Group_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__Excludes__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getExcludesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__0"
    // InternalSat.g:1117:1: rule__Excludes__Group_1__0 : rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 ;
    public final void rule__Excludes__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1121:1: ( rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1 )
            // InternalSat.g:1122:2: rule__Excludes__Group_1__0__Impl rule__Excludes__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Excludes__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0"


    // $ANTLR start "rule__Excludes__Group_1__0__Impl"
    // InternalSat.g:1129:1: rule__Excludes__Group_1__0__Impl : ( () ) ;
    public final void rule__Excludes__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1133:1: ( ( () ) )
            // InternalSat.g:1134:1: ( () )
            {
            // InternalSat.g:1134:1: ( () )
            // InternalSat.g:1135:2: ()
            {
             before(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 
            // InternalSat.g:1136:2: ()
            // InternalSat.g:1136:3: 
            {
            }

             after(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__0__Impl"


    // $ANTLR start "rule__Excludes__Group_1__1"
    // InternalSat.g:1144:1: rule__Excludes__Group_1__1 : rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 ;
    public final void rule__Excludes__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1148:1: ( rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2 )
            // InternalSat.g:1149:2: rule__Excludes__Group_1__1__Impl rule__Excludes__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__Excludes__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1"


    // $ANTLR start "rule__Excludes__Group_1__1__Impl"
    // InternalSat.g:1156:1: rule__Excludes__Group_1__1__Impl : ( 'nand' ) ;
    public final void rule__Excludes__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1160:1: ( ( 'nand' ) )
            // InternalSat.g:1161:1: ( 'nand' )
            {
            // InternalSat.g:1161:1: ( 'nand' )
            // InternalSat.g:1162:2: 'nand'
            {
             before(grammarAccess.getExcludesAccess().getNandKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getExcludesAccess().getNandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__1__Impl"


    // $ANTLR start "rule__Excludes__Group_1__2"
    // InternalSat.g:1171:1: rule__Excludes__Group_1__2 : rule__Excludes__Group_1__2__Impl ;
    public final void rule__Excludes__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1175:1: ( rule__Excludes__Group_1__2__Impl )
            // InternalSat.g:1176:2: rule__Excludes__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2"


    // $ANTLR start "rule__Excludes__Group_1__2__Impl"
    // InternalSat.g:1182:1: rule__Excludes__Group_1__2__Impl : ( ( rule__Excludes__RightAssignment_1_2 ) ) ;
    public final void rule__Excludes__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1186:1: ( ( ( rule__Excludes__RightAssignment_1_2 ) ) )
            // InternalSat.g:1187:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            {
            // InternalSat.g:1187:1: ( ( rule__Excludes__RightAssignment_1_2 ) )
            // InternalSat.g:1188:2: ( rule__Excludes__RightAssignment_1_2 )
            {
             before(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 
            // InternalSat.g:1189:2: ( rule__Excludes__RightAssignment_1_2 )
            // InternalSat.g:1189:3: rule__Excludes__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Excludes__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExcludesAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__Group_1__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // InternalSat.g:1198:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1202:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // InternalSat.g:1203:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // InternalSat.g:1210:1: rule__And__Group__0__Impl : ( ( rule__And__Alternatives_0 ) ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1214:1: ( ( ( rule__And__Alternatives_0 ) ) )
            // InternalSat.g:1215:1: ( ( rule__And__Alternatives_0 ) )
            {
            // InternalSat.g:1215:1: ( ( rule__And__Alternatives_0 ) )
            // InternalSat.g:1216:2: ( rule__And__Alternatives_0 )
            {
             before(grammarAccess.getAndAccess().getAlternatives_0()); 
            // InternalSat.g:1217:2: ( rule__And__Alternatives_0 )
            // InternalSat.g:1217:3: rule__And__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__And__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // InternalSat.g:1225:1: rule__And__Group__1 : rule__And__Group__1__Impl ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1229:1: ( rule__And__Group__1__Impl )
            // InternalSat.g:1230:2: rule__And__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // InternalSat.g:1236:1: rule__And__Group__1__Impl : ( ( rule__And__Group_1__0 )* ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1240:1: ( ( ( rule__And__Group_1__0 )* ) )
            // InternalSat.g:1241:1: ( ( rule__And__Group_1__0 )* )
            {
            // InternalSat.g:1241:1: ( ( rule__And__Group_1__0 )* )
            // InternalSat.g:1242:2: ( rule__And__Group_1__0 )*
            {
             before(grammarAccess.getAndAccess().getGroup_1()); 
            // InternalSat.g:1243:2: ( rule__And__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==18) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalSat.g:1243:3: rule__And__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__And__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getAndAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group_1__0"
    // InternalSat.g:1252:1: rule__And__Group_1__0 : rule__And__Group_1__0__Impl rule__And__Group_1__1 ;
    public final void rule__And__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1256:1: ( rule__And__Group_1__0__Impl rule__And__Group_1__1 )
            // InternalSat.g:1257:2: rule__And__Group_1__0__Impl rule__And__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__And__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0"


    // $ANTLR start "rule__And__Group_1__0__Impl"
    // InternalSat.g:1264:1: rule__And__Group_1__0__Impl : ( () ) ;
    public final void rule__And__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1268:1: ( ( () ) )
            // InternalSat.g:1269:1: ( () )
            {
            // InternalSat.g:1269:1: ( () )
            // InternalSat.g:1270:2: ()
            {
             before(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 
            // InternalSat.g:1271:2: ()
            // InternalSat.g:1271:3: 
            {
            }

             after(grammarAccess.getAndAccess().getAndLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__0__Impl"


    // $ANTLR start "rule__And__Group_1__1"
    // InternalSat.g:1279:1: rule__And__Group_1__1 : rule__And__Group_1__1__Impl rule__And__Group_1__2 ;
    public final void rule__And__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1283:1: ( rule__And__Group_1__1__Impl rule__And__Group_1__2 )
            // InternalSat.g:1284:2: rule__And__Group_1__1__Impl rule__And__Group_1__2
            {
            pushFollow(FOLLOW_5);
            rule__And__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__And__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1"


    // $ANTLR start "rule__And__Group_1__1__Impl"
    // InternalSat.g:1291:1: rule__And__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__And__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1295:1: ( ( 'and' ) )
            // InternalSat.g:1296:1: ( 'and' )
            {
            // InternalSat.g:1296:1: ( 'and' )
            // InternalSat.g:1297:2: 'and'
            {
             before(grammarAccess.getAndAccess().getAndKeyword_1_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getAndAccess().getAndKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__1__Impl"


    // $ANTLR start "rule__And__Group_1__2"
    // InternalSat.g:1306:1: rule__And__Group_1__2 : rule__And__Group_1__2__Impl ;
    public final void rule__And__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1310:1: ( rule__And__Group_1__2__Impl )
            // InternalSat.g:1311:2: rule__And__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__And__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2"


    // $ANTLR start "rule__And__Group_1__2__Impl"
    // InternalSat.g:1317:1: rule__And__Group_1__2__Impl : ( ( rule__And__RightAssignment_1_2 ) ) ;
    public final void rule__And__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1321:1: ( ( ( rule__And__RightAssignment_1_2 ) ) )
            // InternalSat.g:1322:1: ( ( rule__And__RightAssignment_1_2 ) )
            {
            // InternalSat.g:1322:1: ( ( rule__And__RightAssignment_1_2 ) )
            // InternalSat.g:1323:2: ( rule__And__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndAccess().getRightAssignment_1_2()); 
            // InternalSat.g:1324:2: ( rule__And__RightAssignment_1_2 )
            // InternalSat.g:1324:3: rule__And__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group_1__2__Impl"


    // $ANTLR start "rule__Not__Group__0"
    // InternalSat.g:1333:1: rule__Not__Group__0 : rule__Not__Group__0__Impl rule__Not__Group__1 ;
    public final void rule__Not__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1337:1: ( rule__Not__Group__0__Impl rule__Not__Group__1 )
            // InternalSat.g:1338:2: rule__Not__Group__0__Impl rule__Not__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Not__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0"


    // $ANTLR start "rule__Not__Group__0__Impl"
    // InternalSat.g:1345:1: rule__Not__Group__0__Impl : ( '~' ) ;
    public final void rule__Not__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1349:1: ( ( '~' ) )
            // InternalSat.g:1350:1: ( '~' )
            {
            // InternalSat.g:1350:1: ( '~' )
            // InternalSat.g:1351:2: '~'
            {
             before(grammarAccess.getNotAccess().getTildeKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getNotAccess().getTildeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0__Impl"


    // $ANTLR start "rule__Not__Group__1"
    // InternalSat.g:1360:1: rule__Not__Group__1 : rule__Not__Group__1__Impl rule__Not__Group__2 ;
    public final void rule__Not__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1364:1: ( rule__Not__Group__1__Impl rule__Not__Group__2 )
            // InternalSat.g:1365:2: rule__Not__Group__1__Impl rule__Not__Group__2
            {
            pushFollow(FOLLOW_1);
            rule__Not__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Not__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1"


    // $ANTLR start "rule__Not__Group__1__Impl"
    // InternalSat.g:1372:1: rule__Not__Group__1__Impl : ( rulePrimary ) ;
    public final void rule__Not__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1376:1: ( ( rulePrimary ) )
            // InternalSat.g:1377:1: ( rulePrimary )
            {
            // InternalSat.g:1377:1: ( rulePrimary )
            // InternalSat.g:1378:2: rulePrimary
            {
             before(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1__Impl"


    // $ANTLR start "rule__Not__Group__2"
    // InternalSat.g:1387:1: rule__Not__Group__2 : rule__Not__Group__2__Impl ;
    public final void rule__Not__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1391:1: ( rule__Not__Group__2__Impl )
            // InternalSat.g:1392:2: rule__Not__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__2"


    // $ANTLR start "rule__Not__Group__2__Impl"
    // InternalSat.g:1398:1: rule__Not__Group__2__Impl : ( () ) ;
    public final void rule__Not__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1402:1: ( ( () ) )
            // InternalSat.g:1403:1: ( () )
            {
            // InternalSat.g:1403:1: ( () )
            // InternalSat.g:1404:2: ()
            {
             before(grammarAccess.getNotAccess().getNotRightAction_2()); 
            // InternalSat.g:1405:2: ()
            // InternalSat.g:1405:3: 
            {
            }

             after(grammarAccess.getNotAccess().getNotRightAction_2()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__2__Impl"


    // $ANTLR start "rule__PFormula__Group__0"
    // InternalSat.g:1414:1: rule__PFormula__Group__0 : rule__PFormula__Group__0__Impl rule__PFormula__Group__1 ;
    public final void rule__PFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1418:1: ( rule__PFormula__Group__0__Impl rule__PFormula__Group__1 )
            // InternalSat.g:1419:2: rule__PFormula__Group__0__Impl rule__PFormula__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__PFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__0"


    // $ANTLR start "rule__PFormula__Group__0__Impl"
    // InternalSat.g:1426:1: rule__PFormula__Group__0__Impl : ( '(' ) ;
    public final void rule__PFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1430:1: ( ( '(' ) )
            // InternalSat.g:1431:1: ( '(' )
            {
            // InternalSat.g:1431:1: ( '(' )
            // InternalSat.g:1432:2: '('
            {
             before(grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__0__Impl"


    // $ANTLR start "rule__PFormula__Group__1"
    // InternalSat.g:1441:1: rule__PFormula__Group__1 : rule__PFormula__Group__1__Impl rule__PFormula__Group__2 ;
    public final void rule__PFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1445:1: ( rule__PFormula__Group__1__Impl rule__PFormula__Group__2 )
            // InternalSat.g:1446:2: rule__PFormula__Group__1__Impl rule__PFormula__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__PFormula__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PFormula__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__1"


    // $ANTLR start "rule__PFormula__Group__1__Impl"
    // InternalSat.g:1453:1: rule__PFormula__Group__1__Impl : ( ruleBiimplies ) ;
    public final void rule__PFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1457:1: ( ( ruleBiimplies ) )
            // InternalSat.g:1458:1: ( ruleBiimplies )
            {
            // InternalSat.g:1458:1: ( ruleBiimplies )
            // InternalSat.g:1459:2: ruleBiimplies
            {
             before(grammarAccess.getPFormulaAccess().getBiimpliesParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getPFormulaAccess().getBiimpliesParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__1__Impl"


    // $ANTLR start "rule__PFormula__Group__2"
    // InternalSat.g:1468:1: rule__PFormula__Group__2 : rule__PFormula__Group__2__Impl rule__PFormula__Group__3 ;
    public final void rule__PFormula__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1472:1: ( rule__PFormula__Group__2__Impl rule__PFormula__Group__3 )
            // InternalSat.g:1473:2: rule__PFormula__Group__2__Impl rule__PFormula__Group__3
            {
            pushFollow(FOLLOW_19);
            rule__PFormula__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PFormula__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__2"


    // $ANTLR start "rule__PFormula__Group__2__Impl"
    // InternalSat.g:1480:1: rule__PFormula__Group__2__Impl : ( () ) ;
    public final void rule__PFormula__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1484:1: ( ( () ) )
            // InternalSat.g:1485:1: ( () )
            {
            // InternalSat.g:1485:1: ( () )
            // InternalSat.g:1486:2: ()
            {
             before(grammarAccess.getPFormulaAccess().getPFormulaExprAction_2()); 
            // InternalSat.g:1487:2: ()
            // InternalSat.g:1487:3: 
            {
            }

             after(grammarAccess.getPFormulaAccess().getPFormulaExprAction_2()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__2__Impl"


    // $ANTLR start "rule__PFormula__Group__3"
    // InternalSat.g:1495:1: rule__PFormula__Group__3 : rule__PFormula__Group__3__Impl ;
    public final void rule__PFormula__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1499:1: ( rule__PFormula__Group__3__Impl )
            // InternalSat.g:1500:2: rule__PFormula__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PFormula__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__3"


    // $ANTLR start "rule__PFormula__Group__3__Impl"
    // InternalSat.g:1506:1: rule__PFormula__Group__3__Impl : ( ')' ) ;
    public final void rule__PFormula__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1510:1: ( ( ')' ) )
            // InternalSat.g:1511:1: ( ')' )
            {
            // InternalSat.g:1511:1: ( ')' )
            // InternalSat.g:1512:2: ')'
            {
             before(grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PFormula__Group__3__Impl"


    // $ANTLR start "rule__Start__ChecksAssignment_2"
    // InternalSat.g:1522:1: rule__Start__ChecksAssignment_2 : ( ruleCheck ) ;
    public final void rule__Start__ChecksAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1526:1: ( ( ruleCheck ) )
            // InternalSat.g:1527:2: ( ruleCheck )
            {
            // InternalSat.g:1527:2: ( ruleCheck )
            // InternalSat.g:1528:3: ruleCheck
            {
             before(grammarAccess.getStartAccess().getChecksCheckParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCheck();

            state._fsp--;

             after(grammarAccess.getStartAccess().getChecksCheckParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Start__ChecksAssignment_2"


    // $ANTLR start "rule__Check__FormulaAssignment_1"
    // InternalSat.g:1537:1: rule__Check__FormulaAssignment_1 : ( ruleBiimplies ) ;
    public final void rule__Check__FormulaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1541:1: ( ( ruleBiimplies ) )
            // InternalSat.g:1542:2: ( ruleBiimplies )
            {
            // InternalSat.g:1542:2: ( ruleBiimplies )
            // InternalSat.g:1543:3: ruleBiimplies
            {
             before(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBiimplies();

            state._fsp--;

             after(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__FormulaAssignment_1"


    // $ANTLR start "rule__Check__SolverAssignment_3"
    // InternalSat.g:1552:1: rule__Check__SolverAssignment_3 : ( ruleSatSolver ) ;
    public final void rule__Check__SolverAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1556:1: ( ( ruleSatSolver ) )
            // InternalSat.g:1557:2: ( ruleSatSolver )
            {
            // InternalSat.g:1557:2: ( ruleSatSolver )
            // InternalSat.g:1558:3: ruleSatSolver
            {
             before(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleSatSolver();

            state._fsp--;

             after(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__SolverAssignment_3"


    // $ANTLR start "rule__SatSolver__NameAssignment_0"
    // InternalSat.g:1567:1: rule__SatSolver__NameAssignment_0 : ( ( 'SAT4J' ) ) ;
    public final void rule__SatSolver__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1571:1: ( ( ( 'SAT4J' ) ) )
            // InternalSat.g:1572:2: ( ( 'SAT4J' ) )
            {
            // InternalSat.g:1572:2: ( ( 'SAT4J' ) )
            // InternalSat.g:1573:3: ( 'SAT4J' )
            {
             before(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); 
            // InternalSat.g:1574:3: ( 'SAT4J' )
            // InternalSat.g:1575:4: 'SAT4J'
            {
             before(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); 

            }

             after(grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SatSolver__NameAssignment_0"


    // $ANTLR start "rule__SatSolver__NameAssignment_1"
    // InternalSat.g:1586:1: rule__SatSolver__NameAssignment_1 : ( ( 'minisat' ) ) ;
    public final void rule__SatSolver__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1590:1: ( ( ( 'minisat' ) ) )
            // InternalSat.g:1591:2: ( ( 'minisat' ) )
            {
            // InternalSat.g:1591:2: ( ( 'minisat' ) )
            // InternalSat.g:1592:3: ( 'minisat' )
            {
             before(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); 
            // InternalSat.g:1593:3: ( 'minisat' )
            // InternalSat.g:1594:4: 'minisat'
            {
             before(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); 

            }

             after(grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SatSolver__NameAssignment_1"


    // $ANTLR start "rule__SatSolver__NameAssignment_2"
    // InternalSat.g:1605:1: rule__SatSolver__NameAssignment_2 : ( ( 'picosat' ) ) ;
    public final void rule__SatSolver__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1609:1: ( ( ( 'picosat' ) ) )
            // InternalSat.g:1610:2: ( ( 'picosat' ) )
            {
            // InternalSat.g:1610:2: ( ( 'picosat' ) )
            // InternalSat.g:1611:3: ( 'picosat' )
            {
             before(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); 
            // InternalSat.g:1612:3: ( 'picosat' )
            // InternalSat.g:1613:4: 'picosat'
            {
             before(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); 

            }

             after(grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SatSolver__NameAssignment_2"


    // $ANTLR start "rule__Biimplies__RightAssignment_1_2"
    // InternalSat.g:1624:1: rule__Biimplies__RightAssignment_1_2 : ( ruleImplies ) ;
    public final void rule__Biimplies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1628:1: ( ( ruleImplies ) )
            // InternalSat.g:1629:2: ( ruleImplies )
            {
            // InternalSat.g:1629:2: ( ruleImplies )
            // InternalSat.g:1630:3: ruleImplies
            {
             before(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleImplies();

            state._fsp--;

             after(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Biimplies__RightAssignment_1_2"


    // $ANTLR start "rule__Implies__RightAssignment_1_2"
    // InternalSat.g:1639:1: rule__Implies__RightAssignment_1_2 : ( ruleOr ) ;
    public final void rule__Implies__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1643:1: ( ( ruleOr ) )
            // InternalSat.g:1644:2: ( ruleOr )
            {
            // InternalSat.g:1644:2: ( ruleOr )
            // InternalSat.g:1645:3: ruleOr
            {
             before(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOr();

            state._fsp--;

             after(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Implies__RightAssignment_1_2"


    // $ANTLR start "rule__Or__RightAssignment_1_2"
    // InternalSat.g:1654:1: rule__Or__RightAssignment_1_2 : ( ruleExcludes ) ;
    public final void rule__Or__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1658:1: ( ( ruleExcludes ) )
            // InternalSat.g:1659:2: ( ruleExcludes )
            {
            // InternalSat.g:1659:2: ( ruleExcludes )
            // InternalSat.g:1660:3: ruleExcludes
            {
             before(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExcludes();

            state._fsp--;

             after(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_1_2"


    // $ANTLR start "rule__Excludes__RightAssignment_1_2"
    // InternalSat.g:1669:1: rule__Excludes__RightAssignment_1_2 : ( ruleAnd ) ;
    public final void rule__Excludes__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1673:1: ( ( ruleAnd ) )
            // InternalSat.g:1674:2: ( ruleAnd )
            {
            // InternalSat.g:1674:2: ( ruleAnd )
            // InternalSat.g:1675:3: ruleAnd
            {
             before(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAnd();

            state._fsp--;

             after(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Excludes__RightAssignment_1_2"


    // $ANTLR start "rule__And__RightAssignment_1_2"
    // InternalSat.g:1684:1: rule__And__RightAssignment_1_2 : ( ( rule__And__RightAlternatives_1_2_0 ) ) ;
    public final void rule__And__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1688:1: ( ( ( rule__And__RightAlternatives_1_2_0 ) ) )
            // InternalSat.g:1689:2: ( ( rule__And__RightAlternatives_1_2_0 ) )
            {
            // InternalSat.g:1689:2: ( ( rule__And__RightAlternatives_1_2_0 ) )
            // InternalSat.g:1690:3: ( rule__And__RightAlternatives_1_2_0 )
            {
             before(grammarAccess.getAndAccess().getRightAlternatives_1_2_0()); 
            // InternalSat.g:1691:3: ( rule__And__RightAlternatives_1_2_0 )
            // InternalSat.g:1691:4: rule__And__RightAlternatives_1_2_0
            {
            pushFollow(FOLLOW_2);
            rule__And__RightAlternatives_1_2_0();

            state._fsp--;


            }

             after(grammarAccess.getAndAccess().getRightAlternatives_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_1_2"


    // $ANTLR start "rule__Variable__IdAssignment"
    // InternalSat.g:1699:1: rule__Variable__IdAssignment : ( RULE_ID ) ;
    public final void rule__Variable__IdAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSat.g:1703:1: ( ( RULE_ID ) )
            // InternalSat.g:1704:2: ( RULE_ID )
            {
            // InternalSat.g:1704:2: ( RULE_ID )
            // InternalSat.g:1705:3: RULE_ID
            {
             before(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__IdAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000180010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000001C00000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000200000L});

}