package org.sif.dsl.tests;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.sif.dsl.MinisatSolver;
import org.sif.dsl.SAT4JSolver;
import org.sif.dsl.sat.Check;
import org.sif.dsl.sat.Start;
import org.sif.dsl.tests.SatInjectorProvider;

@ExtendWith(InjectionExtension.class)
@InjectWith(SatInjectorProvider.class)
@SuppressWarnings("all")
public class SatSolverEquivalence {
  @Inject
  private ParseHelper<Start> parseHelper;
  
  private final SAT4JSolver sat4jSolver = new SAT4JSolver();
  
  private final MinisatSolver minisatSolver = new MinisatSolver();
  
  @Test
  public void equivalenceTest() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Check A and B => (C or A) <=> D and (~B nand C) using SAT4J;");
      _builder.newLine();
      final Start result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      final Check check = result.getChecks().get(0);
      String _checkSatisfiability = this.sat4jSolver.checkSatisfiability(check);
      String _checkSatisfiability_1 = this.minisatSolver.checkSatisfiability(check);
      boolean _equals = Objects.equal(_checkSatisfiability, _checkSatisfiability_1);
      Assertions.assertTrue(_equals);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
