package org.sif.dsl.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.sif.dsl.sat.Start
import org.sif.dsl.SAT4JSolver
import org.sif.dsl.MinisatSolver

@ExtendWith(InjectionExtension)
@InjectWith(SatInjectorProvider)
class SatSolverEquivalence {
	@Inject
	ParseHelper<Start> parseHelper
	
	val sat4jSolver = new SAT4JSolver()
	val minisatSolver = new MinisatSolver()
	
	@Test
	def void equivalenceTest() {
		val result = parseHelper.parse('''
			Check A and B => (C or A) <=> D and (~B nand C) using SAT4J;
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: «errors.join(", ")»''')
		val check = result.checks.get(0)
		Assertions.assertTrue(sat4jSolver.checkSatisfiability(check) == minisatSolver.checkSatisfiability(check))
	}
}