package org.sif.dsl

import org.sat4j.minisat.SolverFactory
import org.sat4j.reader.DimacsReader
import java.io.ByteArrayInputStream
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException

class SAT4JSolver extends Solver {
	override check(String fileContents) {
		val solver = SolverFactory.newDefault();
		solver.setTimeout(3600); // 1 hour timeout
		val reader = new DimacsReader(solver);
		
		try {
			val problem = reader.parseInstance(new ByteArrayInputStream(fileContents.getBytes("US-ASCII")));
	        if (problem.isSatisfiable()) {
	            true
	        } else {
	            false
	        }			
		} catch (ContradictionException e) {
            false
        } catch (TimeoutException e) {
            false
        }
	}
}