package org.sif.dsl

import java.util.Vector
import org.eclipse.emf.ecore.util.EcoreUtil.Copier
import org.eclipse.emf.ecore.EObject
import java.util.HashMap
import org.sif.dsl.sat.Formula
import org.sif.dsl.sat.Biimplies
import org.sif.dsl.sat.Implies
import org.sif.dsl.sat.Excludes
import org.sif.dsl.sat.PFormula
import org.sif.dsl.sat.Or
import org.sif.dsl.sat.Not
import org.sif.dsl.sat.And
import org.sif.dsl.sat.SatFactory
import org.sif.dsl.sat.Check

abstract class Solver {
	val copier = new Copier()
	
	def <T extends EObject> copy(T obj) {
		copier.copy(obj) as T
	}
	
	// Extract the clauses of a CNF formula f
	def Vector<Formula> extractClauses(Formula f) {
		switch f {
			Biimplies,
			Implies,
			Excludes,
			PFormula:
				throw new RuntimeException("Unexpected clause in CNF formula")
			Or,
			Not: {
				val res = new Vector<Formula>()
				res.add(f)
				res
			}
			And: {
				val lhs = extractClauses(f.left)
				val rhs = extractClauses(f.right)
				val res = new Vector<Formula>()
				res.addAll(lhs)
				res.addAll(rhs)
				res
			}
			default: {
				if(f.id !== null) {
					val res = new Vector<Formula>()
					res.add(f)
					return res
				}
				throw new RuntimeException("Unexpected clause in CNF formula")
			}
		}
	}
	
	def Formula toAnd(Vector<Formula> clauseList) {
		if(clauseList.size() < 1)
			throw new RuntimeException("Empty clause list")
	 	else if(clauseList.size() == 1) {
	 		clauseList.get(0)
	 	}
	 	else {
	 		val res = SatFactory.eINSTANCE.createAnd()
	 		res.left = clauseList.remove(0)
	 		val rhs = toAnd(clauseList)
	 		res.right = rhs
	 		res
	 	}
	}
	
	def Formula toCNF(Formula f) {
		switch f {
			Biimplies: {
				
				// If f is of the form A <=> B then
				// return toCNF((A => B) and (B => A))
				val lImpl = SatFactory.eINSTANCE.createImplies()
				lImpl.left = f.left.copy
				lImpl.right = f.right.copy
				val rImpl = SatFactory.eINSTANCE.createImplies()
				rImpl.left = f.right.copy
				rImpl.right = f.left.copy
				val res = SatFactory.eINSTANCE.createAnd()
				res.left = lImpl
				res.right = rImpl
				toCNF(res)
			}
			Implies: {
				// If f is of the form A => B then
				// return toCNF(~A or B)
				val lNot = SatFactory.eINSTANCE.createNot()
				lNot.right = f.left.copy
				val res = SatFactory.eINSTANCE.createOr()
				res.left = lNot
				res.right = f.right.copy
				toCNF(res)
			}
			Or: {
				// If f has the form A or B then
				// toCNF(A) is of the form A1 and A2 and ... and An
				// toCNF(B) is of the form B1 and B2 and ... and Bm
				// return (A1 or B1) and (A1 or B2) and ... and (A2 or B1) and (A2 or B2) and ... and (An or Bm)
				val lhsClauses = extractClauses(toCNF(f.left.copy))
				val rhsClauses = extractClauses(toCNF(f.right.copy))
				val clauses = new Vector<Formula>()
				for(lc : lhsClauses) {
					for(rc : rhsClauses) {
						val c = SatFactory.eINSTANCE.createOr()
						c.left = lc.copy
						c.right = rc.copy
						clauses.add(c)
					}
				}
				toAnd(clauses)
			}
			Excludes: {
				// If f is of the form A nand B then
				// return toCNF(~(A and B))
				val and = SatFactory.eINSTANCE.createAnd()
				and.left = f.left.copy
				and.right = f.right.copy
				val res = SatFactory.eINSTANCE.createNot()
				res.right = and
				toCNF(res)
			}
			And: {
				// If f is of the form A and B then
				// toCNF(A) is of the form A1 and A2 and ... and An
				// toCNF(B) is of the form B1 and B2 and ... and Bm
				// return A1 and ... and An and B1 and ... and Bm
				val lAnd = toCNF(f.left.copy)
				val rAnd = toCNF(f.right.copy)
				val res = SatFactory.eINSTANCE.createAnd()
				res.left = lAnd
				res.right = rAnd
				res
			}
			Not: {
				val rhs = f.right
				switch rhs {
					Biimplies: {
						val lAnd = SatFactory.eINSTANCE.createAnd()
						lAnd.left = rhs.left.copy
						lAnd.right = rhs.right.copy
						val lNot = SatFactory.eINSTANCE.createNot()
						lNot.right = rhs.left.copy
						val rNot = SatFactory.eINSTANCE.createNot()
						lNot.right = rhs.right.copy
						val rAnd = SatFactory.eINSTANCE.createAnd()
						rAnd.left = lNot
						rAnd.right = rNot
						val rOr = SatFactory.eINSTANCE.createOr()
						rOr.left = lAnd
						rOr.right = rAnd
						val res = SatFactory.eINSTANCE.createNot()
						res.right = rOr
						toCNF(res)
					}
					Implies: {
						val lNot = SatFactory.eINSTANCE.createNot()
						lNot.right = rhs.left.copy
						val rOr = SatFactory.eINSTANCE.createOr()
						rOr.left = lNot
						rOr.right = rhs.right.copy
						val res = SatFactory.eINSTANCE.createNot()
						res.right = rOr
						toCNF(res)
					}
					Or: {
						// If f is of the form ~(A or B) then
						// return toCNF(~A and ~B)
						val lNot = SatFactory.eINSTANCE.createNot()
						lNot.right = rhs.left.copy
						val rNot = SatFactory.eINSTANCE.createNot()
						rNot.right = rhs.right.copy
						val res = SatFactory.eINSTANCE.createAnd()
						res.left = lNot
						res.right = rNot
						toCNF(res)
					}
					Excludes: {
						// If f is of the form ~(A nand B) then
						// return toCNF(A and B)
						val res = SatFactory.eINSTANCE.createAnd()
						res.left = rhs.left.copy
						res.right = rhs.right.copy
						toCNF(res)
					}
					And: {
						// If f is of the form ~(A and B) then
						// return toCNF(~A or ~B)
						val lNot = SatFactory.eINSTANCE.createNot()
						lNot.right = rhs.left.copy
						val rNot = SatFactory.eINSTANCE.createNot()
						rNot.right = rhs.right.copy
						val res = SatFactory.eINSTANCE.createOr()
						res.left = lNot
						res.right = rNot
						toCNF(res)
					}
					Not: {
						// If f is of the form ~(~A) then
						// return toCNF(A)
						toCNF(rhs.right.copy)
					}
					PFormula: {
						// If f is of the form ~(A) then
						// return toCNF(~A)
						val res = SatFactory.eINSTANCE.createNot()
						res.right = rhs.expr.copy
						toCNF(res)
					}
					default: {
						if(rhs.id !== null)
							f
						else
							throw new RuntimeException("Unexpected error while converting to CNF")
					}
				}
			}
			PFormula: {
				toCNF(f.expr.copy)
			}
			default: {
				if(f.id !== null)
					f
				else
					throw new RuntimeException("Unexpected error while converting to CNF")
			}
		}
	}
	
	def HashMap<String, Integer> getVariableIDs(Formula f, HashMap<String, Integer> currentIDs) {
		switch f {
			Biimplies: {
				val lhs = getVariableIDs(f.left, currentIDs)
				currentIDs.putAll(lhs)
				getVariableIDs(f.right, currentIDs)
			}
			Implies: {
				val lhs = getVariableIDs(f.left, currentIDs)
				currentIDs.putAll(lhs)
				getVariableIDs(f.right, currentIDs)
			}
			Or: {
				val lhs = getVariableIDs(f.left, currentIDs)
				currentIDs.putAll(lhs)
				getVariableIDs(f.right, currentIDs)
			}
			Excludes: {
				val lhs = getVariableIDs(f.left, currentIDs)
				currentIDs.putAll(lhs)
				getVariableIDs(f.right, currentIDs)
			}
			And: {
				val lhs = getVariableIDs(f.left, currentIDs)
				currentIDs.putAll(lhs)
				getVariableIDs(f.right, currentIDs)
			}
			Not: {
				getVariableIDs(f.right, currentIDs)
			}
			PFormula: {
				getVariableIDs(f.expr, currentIDs)
			}
			default: {
				if(f.id !== null) {
					if(!currentIDs.containsKey(f.id)) {
						currentIDs.put(f.id, currentIDs.size() + 1)
					}
					currentIDs
				}
				else {
					throw new RuntimeException("Unexpected formula")
				}
			}
		}
	}
	
	def Integer getNumClauses(Formula f) {
		switch f {
			Biimplies,
			Implies,
			Excludes:
				throw new RuntimeException("Not a CNF formula")
			And: {
				val lhsContrib = switch f.left {
					And: 0
					default: 1
				}
				val rhsContrib = switch f.right {
					And: 0
					default: 1
				}
				lhsContrib + rhsContrib + getNumClauses(f.left) + getNumClauses(f.right)
			}
			PFormula: getNumClauses(f.expr)
			default: 0
		}
	}
	
	def String satToDimacs(Formula f, HashMap<String, Integer> variableIDs) {
		switch f {
			Biimplies,
			Implies,
			Excludes: throw new RuntimeException("Not a CNF formula")
			Or: {
				var lhs = satToDimacs(f.left, variableIDs)
				var rhs = satToDimacs(f.right, variableIDs)
				lhs + ' ' + rhs
			}
			And: {
				var lhs = satToDimacs(f.left, variableIDs)
				var rhs = satToDimacs(f.right, variableIDs)
				var lhsStr = if(f.left instanceof And) lhs else lhs + ' 0\n'
				var rhsStr = if(f.right instanceof And) rhs else rhs + ' 0\n'
				lhsStr + rhsStr
			}
			Not: {
				'-' + satToDimacs(f.right, variableIDs)
			}
			PFormula: {
				satToDimacs(f.expr, variableIDs)
			}
			default: {
				if(f.id !== null)
					variableIDs.get(f.id).toString
				else
					throw new RuntimeException("Unexpected formula")
			}
		}
	}
	
	def checkSatisfiability(Check c) {
		val cnfFormula = toCNF(c.formula)
		val variableIDs = getVariableIDs(cnfFormula, new HashMap<String, Integer>())
		val numVariables = variableIDs.size()
		val numClauses = getNumClauses(cnfFormula)
		val dimacsStr = satToDimacs(cnfFormula, variableIDs)
		val dimacsFileContents = String.format("p cnf %d %d\n", numVariables, if(numClauses > 0) numClauses else 1) + dimacsStr
		
		if(check(dimacsFileContents))
			"SATISFIABLE"
		else
			"UNSATISFIABLE"
	}
	
	def Boolean check(String fileContents)
}