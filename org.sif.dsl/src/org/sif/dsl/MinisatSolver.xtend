package org.sif.dsl

import java.util.ArrayList
import java.io.BufferedReader
import java.io.InputStreamReader

class MinisatSolver extends Solver {
	override check(String fileContents) {
		var commands = new ArrayList<String>();
	    commands.add("/bin/sh");
	    commands.add("-c");
	    commands.add("echo \"" + fileContents + "\" | /usr/bin/minisat | tail -1");
		val p = Runtime.getRuntime().exec(commands)
		p.waitFor()
					
		val preader = new BufferedReader(new InputStreamReader(p.getInputStream()))
		val res = preader.readLine()
		
		res == 'SATISFIABLE'
	}	
}