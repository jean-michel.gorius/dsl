package org.sif.dsl.launcher

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.IAdaptable
import org.eclipse.debug.ui.ILaunchShortcut
import org.eclipse.jface.viewers.ISelection
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorPart

class LaunchShortcut implements ILaunchShortcut {
	IFile currFile

	override void launch(ISelection selection, String mode) {
		if (selection instanceof IStructuredSelection) {
			val IStructuredSelection structuredSelection = (selection as IStructuredSelection)
			val Object object = structuredSelection.getFirstElement()
			if (object instanceof IAdaptable) {
				currFile = ((object as IAdaptable)).getAdapter(IResource) as IFile
				var InputStream in = null
				try {
					in = currFile.getContents()
				} catch (CoreException e) {
					e.printStackTrace()
				}

				var BufferedReader reader = new BufferedReader(new InputStreamReader(in))
				var StringBuilder out = new StringBuilder()
				var String line
				try {
					while ((line = reader.readLine()) !== null) {
						out.append(line)
					}
				} catch (IOException e) {
					e.printStackTrace()
				}

				launch(out.toString())
			}
		}
	}

	override void launch(IEditorPart editor, String mode) {
		val IEditorInput input = editor.getEditorInput()
		currFile = input.getAdapter(IFile) as IFile
		var InputStream in = null
		try {
			in = currFile.getContents()
		} catch (CoreException e) {
			e.printStackTrace()
		}

		var BufferedReader reader = new BufferedReader(new InputStreamReader(in))
		var StringBuilder out = new StringBuilder()
		var String line
		try {
			while ((line = reader.readLine()) !== null) {
				out.append(line)
			}
		} catch (IOException e) {
			e.printStackTrace()
		}

		launch(out.toString())
	}

	def private void launch(String fileContents) {
		(new SolverLauncher()).launch(fileContents)
	}
}
