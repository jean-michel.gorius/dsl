package org.sif.dsl.launcher

import java.io.ByteArrayInputStream
import org.sif.dsl.SatStandaloneSetup
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.emf.common.util.URI
import org.eclipse.ui.console.ConsolePlugin
import org.eclipse.ui.console.MessageConsole
import org.eclipse.ui.console.MessageConsoleStream
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.console.IConsoleConstants
import org.eclipse.ui.console.IConsoleView
import org.eclipse.ui.PartInitException
import org.sif.dsl.sat.Start
import org.sif.dsl.SAT4JSolver
import org.sif.dsl.MinisatSolver
import org.sif.dsl.sat.Formula
import org.sif.dsl.sat.Biimplies
import org.sif.dsl.sat.Implies
import org.sif.dsl.sat.Or
import org.sif.dsl.sat.Excludes
import org.sif.dsl.sat.And
import org.sif.dsl.sat.Not
import org.sif.dsl.sat.PFormula
import org.sif.dsl.PicosatSolver

class SolverLauncher {
	val injector = new SatStandaloneSetup().createInjectorAndDoEMFRegistration()
	
	def static MessageConsole findConsole(String name) {
		if (ConsolePlugin.getDefault() === null)
			return null;
		val plugin = ConsolePlugin.getDefault();
		val conMan = plugin.getConsoleManager();
		val existing = conMan.getConsoles();
		for (var i = 0; i < existing.length; i++)
			if (name.equals(existing.get(i).getName())) {
				conMan.showConsoleView(existing.get(i));
				return existing.get(i) as MessageConsole;
			}
		// No console found, so create a new one
		val myConsole = new MessageConsole(name, null);
		conMan.addConsoles(#[myConsole]);
		return myConsole;
	}

	def MessageConsoleStream getMessageStream() {
		val myConsole = findConsole("Satisfiability check");
		if (myConsole !== null) {
			val wb = PlatformUI.getWorkbench();
			val win = wb.getActiveWorkbenchWindow();
			val page = win.getActivePage();
			val id = IConsoleConstants.ID_CONSOLE_VIEW;
			var view = null as IConsoleView;
			try {
				view = page.showView(id) as IConsoleView;
				view.display(myConsole);
				return myConsole.newMessageStream();
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	def String toString(Formula f) {
		switch f {
			Biimplies: {
				'''(«toString(f.left)» <=> «toString(f.right)»)'''
			}
			Implies: {
				'''(«toString(f.left)» => «toString(f.right)»)'''
			}
			Or: {
				'''(«toString(f.left)» || «toString(f.right)»)'''
			}
			Excludes: {
				'''(«toString(f.left)» ~& «toString(f.right)»)'''
			}
			And: {
				'''(«toString(f.left)» && «toString(f.right)»)'''
			}
			Not: {
				'''~«toString(f.right)»'''
			}
			PFormula: {
				'''(«toString(f.expr)»)'''
			}
			default: {
				if(f.id !== null)
					f.id
				else
					throw new RuntimeException("Unexpected expression");
			}
		}
	}
	
	def launch(String fileContents) {
		val resourceSet = injector.getInstance(typeof(XtextResourceSet))
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE)
		val resource = resourceSet.createResource(URI.createURI("dummy:/file.sat"))
		val in = new ByteArrayInputStream(fileContents.getBytes())
		resource.load(in, resourceSet.getLoadOptions())
		val result = resource.getContents.get(0) as Start
		
		val outStream = getMessageStream()
		val sat4jSolver = new SAT4JSolver()
		val minisatSolver = new MinisatSolver()
		val picosatSolver = new PicosatSolver()
		
		for(check : result.checks) {
			outStream.println("Checking satisfiability of " + toString(check.formula) + " [" + check.solver.name + "]")
			if(check.solver.name == "SAT4J")
				outStream.println(sat4jSolver.checkSatisfiability(check))
			else if(check.solver.name == "minisat")
				outStream.println(minisatSolver.checkSatisfiability(check))
			else if(check.solver.name == "picosat")
				outStream.println(picosatSolver.checkSatisfiability(check))
			else
				outStream.println("Unknown solver " + check.solver.name)
		}
	}
}
