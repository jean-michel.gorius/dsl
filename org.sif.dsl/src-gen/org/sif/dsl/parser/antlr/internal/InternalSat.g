/*
 * generated by Xtext 2.15.0
 */
grammar InternalSat;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package org.sif.dsl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package org.sif.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.sif.dsl.services.SatGrammarAccess;

}

@parser::members {

 	private SatGrammarAccess grammarAccess;

    public InternalSatParser(TokenStream input, SatGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "Start";
   	}

   	@Override
   	protected SatGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleStart
entryRuleStart returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getStartRule()); }
	iv_ruleStart=ruleStart
	{ $current=$iv_ruleStart.current; }
	EOF;

// Rule Start
ruleStart returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getStartAccess().getCheckParserRuleCall_0());
		}
		this_Check_0=ruleCheck
		{
			$current = $this_Check_0.current;
			afterParserOrEnumRuleCall();
		}
		(
			{
				$current = forceCreateModelElementAndAdd(
					grammarAccess.getStartAccess().getStartChecksAction_1(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getStartAccess().getChecksCheckParserRuleCall_2_0());
				}
				lv_checks_2_0=ruleCheck
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getStartRule());
					}
					add(
						$current,
						"checks",
						lv_checks_2_0,
						"org.sif.dsl.Sat.Check");
					afterParserOrEnumRuleCall();
				}
			)
		)*
	)
;

// Entry rule entryRuleCheck
entryRuleCheck returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getCheckRule()); }
	iv_ruleCheck=ruleCheck
	{ $current=$iv_ruleCheck.current; }
	EOF;

// Rule Check
ruleCheck returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='Check'
		{
			newLeafNode(otherlv_0, grammarAccess.getCheckAccess().getCheckKeyword_0());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0());
				}
				lv_formula_1_0=ruleBiimplies
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getCheckRule());
					}
					set(
						$current,
						"formula",
						lv_formula_1_0,
						"org.sif.dsl.Sat.Biimplies");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_2='using'
		{
			newLeafNode(otherlv_2, grammarAccess.getCheckAccess().getUsingKeyword_2());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0());
				}
				lv_solver_3_0=ruleSatSolver
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getCheckRule());
					}
					set(
						$current,
						"solver",
						lv_solver_3_0,
						"org.sif.dsl.Sat.SatSolver");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_4=';'
		{
			newLeafNode(otherlv_4, grammarAccess.getCheckAccess().getSemicolonKeyword_4());
		}
	)
;

// Entry rule entryRuleSatSolver
entryRuleSatSolver returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getSatSolverRule()); }
	iv_ruleSatSolver=ruleSatSolver
	{ $current=$iv_ruleSatSolver.current; }
	EOF;

// Rule SatSolver
ruleSatSolver returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			(
				lv_name_0_0='SAT4J'
				{
					newLeafNode(lv_name_0_0, grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSatSolverRule());
					}
					setWithLastConsumed($current, "name", lv_name_0_0, "SAT4J");
				}
			)
		)
		    |
		(
			(
				lv_name_1_0='minisat'
				{
					newLeafNode(lv_name_1_0, grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSatSolverRule());
					}
					setWithLastConsumed($current, "name", lv_name_1_0, "minisat");
				}
			)
		)
		    |
		(
			(
				lv_name_2_0='picosat'
				{
					newLeafNode(lv_name_2_0, grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getSatSolverRule());
					}
					setWithLastConsumed($current, "name", lv_name_2_0, "picosat");
				}
			)
		)
	)
;

// Entry rule entryRuleBiimplies
entryRuleBiimplies returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getBiimpliesRule()); }
	iv_ruleBiimplies=ruleBiimplies
	{ $current=$iv_ruleBiimplies.current; }
	EOF;

// Rule Biimplies
ruleBiimplies returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
		}
		this_Implies_0=ruleImplies
		{
			$current = $this_Implies_0.current;
			afterParserOrEnumRuleCall();
		}
		(
			(
				{
					$current = forceCreateModelElementAndSet(
						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
						$current);
				}
			)
			otherlv_2='<=>'
			{
				newLeafNode(otherlv_2, grammarAccess.getBiimpliesAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
					}
					lv_right_3_0=ruleImplies
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getBiimpliesRule());
						}
						set(
							$current,
							"right",
							lv_right_3_0,
							"org.sif.dsl.Sat.Implies");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
	)
;

// Entry rule entryRuleImplies
entryRuleImplies returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getImpliesRule()); }
	iv_ruleImplies=ruleImplies
	{ $current=$iv_ruleImplies.current; }
	EOF;

// Rule Implies
ruleImplies returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getImpliesAccess().getOrParserRuleCall_0());
		}
		this_Or_0=ruleOr
		{
			$current = $this_Or_0.current;
			afterParserOrEnumRuleCall();
		}
		(
			(
				{
					$current = forceCreateModelElementAndSet(
						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
						$current);
				}
			)
			otherlv_2='=>'
			{
				newLeafNode(otherlv_2, grammarAccess.getImpliesAccess().getEqualsSignGreaterThanSignKeyword_1_1());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0());
					}
					lv_right_3_0=ruleOr
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getImpliesRule());
						}
						set(
							$current,
							"right",
							lv_right_3_0,
							"org.sif.dsl.Sat.Or");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
	)
;

// Entry rule entryRuleOr
entryRuleOr returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getOrRule()); }
	iv_ruleOr=ruleOr
	{ $current=$iv_ruleOr.current; }
	EOF;

// Rule Or
ruleOr returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getOrAccess().getExcludesParserRuleCall_0());
		}
		this_Excludes_0=ruleExcludes
		{
			$current = $this_Excludes_0.current;
			afterParserOrEnumRuleCall();
		}
		(
			(
				{
					$current = forceCreateModelElementAndSet(
						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
						$current);
				}
			)
			otherlv_2='or'
			{
				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getOrKeyword_1_1());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0());
					}
					lv_right_3_0=ruleExcludes
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getOrRule());
						}
						set(
							$current,
							"right",
							lv_right_3_0,
							"org.sif.dsl.Sat.Excludes");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
	)
;

// Entry rule entryRuleExcludes
entryRuleExcludes returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getExcludesRule()); }
	iv_ruleExcludes=ruleExcludes
	{ $current=$iv_ruleExcludes.current; }
	EOF;

// Rule Excludes
ruleExcludes returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getExcludesAccess().getAndParserRuleCall_0());
		}
		this_And_0=ruleAnd
		{
			$current = $this_And_0.current;
			afterParserOrEnumRuleCall();
		}
		(
			(
				{
					$current = forceCreateModelElementAndSet(
						grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(),
						$current);
				}
			)
			otherlv_2='nand'
			{
				newLeafNode(otherlv_2, grammarAccess.getExcludesAccess().getNandKeyword_1_1());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0());
					}
					lv_right_3_0=ruleAnd
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getExcludesRule());
						}
						set(
							$current,
							"right",
							lv_right_3_0,
							"org.sif.dsl.Sat.And");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
	)
;

// Entry rule entryRuleAnd
entryRuleAnd returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getAndRule()); }
	iv_ruleAnd=ruleAnd
	{ $current=$iv_ruleAnd.current; }
	EOF;

// Rule And
ruleAnd returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				newCompositeNode(grammarAccess.getAndAccess().getNotParserRuleCall_0_0());
			}
			this_Not_0=ruleNot
			{
				$current = $this_Not_0.current;
				afterParserOrEnumRuleCall();
			}
			    |
			{
				newCompositeNode(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0_1());
			}
			this_Primary_1=rulePrimary
			{
				$current = $this_Primary_1.current;
				afterParserOrEnumRuleCall();
			}
		)
		(
			(
				{
					$current = forceCreateModelElementAndSet(
						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
						$current);
				}
			)
			otherlv_3='and'
			{
				newLeafNode(otherlv_3, grammarAccess.getAndAccess().getAndKeyword_1_1());
			}
			(
				(
					(
						{
							newCompositeNode(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0_0());
						}
						lv_right_4_1=ruleNot
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getAndRule());
							}
							set(
								$current,
								"right",
								lv_right_4_1,
								"org.sif.dsl.Sat.Not");
							afterParserOrEnumRuleCall();
						}
						    |
						{
							newCompositeNode(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0_1());
						}
						lv_right_4_2=rulePrimary
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getAndRule());
							}
							set(
								$current,
								"right",
								lv_right_4_2,
								"org.sif.dsl.Sat.Primary");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)
		)*
	)
;

// Entry rule entryRuleNot
entryRuleNot returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getNotRule()); }
	iv_ruleNot=ruleNot
	{ $current=$iv_ruleNot.current; }
	EOF;

// Rule Not
ruleNot returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='~'
		{
			newLeafNode(otherlv_0, grammarAccess.getNotAccess().getTildeKeyword_0());
		}
		{
			newCompositeNode(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1());
		}
		this_Primary_1=rulePrimary
		{
			$current = $this_Primary_1.current;
			afterParserOrEnumRuleCall();
		}
		(
			{
				$current = forceCreateModelElementAndSet(
					grammarAccess.getNotAccess().getNotRightAction_2(),
					$current);
			}
		)
	)
;

// Entry rule entryRulePrimary
entryRulePrimary returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getPrimaryRule()); }
	iv_rulePrimary=rulePrimary
	{ $current=$iv_rulePrimary.current; }
	EOF;

// Rule Primary
rulePrimary returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		{
			newCompositeNode(grammarAccess.getPrimaryAccess().getVariableParserRuleCall_0());
		}
		this_Variable_0=ruleVariable
		{
			$current = $this_Variable_0.current;
			afterParserOrEnumRuleCall();
		}
		    |
		{
			newCompositeNode(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1());
		}
		this_PFormula_1=rulePFormula
		{
			$current = $this_PFormula_1.current;
			afterParserOrEnumRuleCall();
		}
	)
;

// Entry rule entryRuleVariable
entryRuleVariable returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getVariableRule()); }
	iv_ruleVariable=ruleVariable
	{ $current=$iv_ruleVariable.current; }
	EOF;

// Rule Variable
ruleVariable returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			lv_id_0_0=RULE_ID
			{
				newLeafNode(lv_id_0_0, grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0());
			}
			{
				if ($current==null) {
					$current = createModelElement(grammarAccess.getVariableRule());
				}
				setWithLastConsumed(
					$current,
					"id",
					lv_id_0_0,
					"org.eclipse.xtext.common.Terminals.ID");
			}
		)
	)
;

// Entry rule entryRulePFormula
entryRulePFormula returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getPFormulaRule()); }
	iv_rulePFormula=rulePFormula
	{ $current=$iv_rulePFormula.current; }
	EOF;

// Rule PFormula
rulePFormula returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		otherlv_0='('
		{
			newLeafNode(otherlv_0, grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0());
		}
		{
			newCompositeNode(grammarAccess.getPFormulaAccess().getBiimpliesParserRuleCall_1());
		}
		this_Biimplies_1=ruleBiimplies
		{
			$current = $this_Biimplies_1.current;
			afterParserOrEnumRuleCall();
		}
		(
			{
				$current = forceCreateModelElementAndSet(
					grammarAccess.getPFormulaAccess().getPFormulaExprAction_2(),
					$current);
			}
		)
		otherlv_3=')'
		{
			newLeafNode(otherlv_3, grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3());
		}
	)
;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
