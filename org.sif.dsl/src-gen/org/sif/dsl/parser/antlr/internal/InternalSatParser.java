package org.sif.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.sif.dsl.services.SatGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSatParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Check'", "'using'", "';'", "'SAT4J'", "'minisat'", "'picosat'", "'<=>'", "'=>'", "'or'", "'nand'", "'and'", "'~'", "'('", "')'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSatParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSatParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSatParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSat.g"; }



     	private SatGrammarAccess grammarAccess;

        public InternalSatParser(TokenStream input, SatGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Start";
       	}

       	@Override
       	protected SatGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStart"
    // InternalSat.g:64:1: entryRuleStart returns [EObject current=null] : iv_ruleStart= ruleStart EOF ;
    public final EObject entryRuleStart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStart = null;


        try {
            // InternalSat.g:64:46: (iv_ruleStart= ruleStart EOF )
            // InternalSat.g:65:2: iv_ruleStart= ruleStart EOF
            {
             newCompositeNode(grammarAccess.getStartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStart=ruleStart();

            state._fsp--;

             current =iv_ruleStart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStart"


    // $ANTLR start "ruleStart"
    // InternalSat.g:71:1: ruleStart returns [EObject current=null] : (this_Check_0= ruleCheck () ( (lv_checks_2_0= ruleCheck ) )* ) ;
    public final EObject ruleStart() throws RecognitionException {
        EObject current = null;

        EObject this_Check_0 = null;

        EObject lv_checks_2_0 = null;



        	enterRule();

        try {
            // InternalSat.g:77:2: ( (this_Check_0= ruleCheck () ( (lv_checks_2_0= ruleCheck ) )* ) )
            // InternalSat.g:78:2: (this_Check_0= ruleCheck () ( (lv_checks_2_0= ruleCheck ) )* )
            {
            // InternalSat.g:78:2: (this_Check_0= ruleCheck () ( (lv_checks_2_0= ruleCheck ) )* )
            // InternalSat.g:79:3: this_Check_0= ruleCheck () ( (lv_checks_2_0= ruleCheck ) )*
            {

            			newCompositeNode(grammarAccess.getStartAccess().getCheckParserRuleCall_0());
            		
            pushFollow(FOLLOW_3);
            this_Check_0=ruleCheck();

            state._fsp--;


            			current = this_Check_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:87:3: ()
            // InternalSat.g:88:4: 
            {

            				current = forceCreateModelElementAndAdd(
            					grammarAccess.getStartAccess().getStartChecksAction_1(),
            					current);
            			

            }

            // InternalSat.g:94:3: ( (lv_checks_2_0= ruleCheck ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSat.g:95:4: (lv_checks_2_0= ruleCheck )
            	    {
            	    // InternalSat.g:95:4: (lv_checks_2_0= ruleCheck )
            	    // InternalSat.g:96:5: lv_checks_2_0= ruleCheck
            	    {

            	    					newCompositeNode(grammarAccess.getStartAccess().getChecksCheckParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_checks_2_0=ruleCheck();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStartRule());
            	    					}
            	    					add(
            	    						current,
            	    						"checks",
            	    						lv_checks_2_0,
            	    						"org.sif.dsl.Sat.Check");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStart"


    // $ANTLR start "entryRuleCheck"
    // InternalSat.g:117:1: entryRuleCheck returns [EObject current=null] : iv_ruleCheck= ruleCheck EOF ;
    public final EObject entryRuleCheck() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheck = null;


        try {
            // InternalSat.g:117:46: (iv_ruleCheck= ruleCheck EOF )
            // InternalSat.g:118:2: iv_ruleCheck= ruleCheck EOF
            {
             newCompositeNode(grammarAccess.getCheckRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCheck=ruleCheck();

            state._fsp--;

             current =iv_ruleCheck; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // InternalSat.g:124:1: ruleCheck returns [EObject current=null] : (otherlv_0= 'Check' ( (lv_formula_1_0= ruleBiimplies ) ) otherlv_2= 'using' ( (lv_solver_3_0= ruleSatSolver ) ) otherlv_4= ';' ) ;
    public final EObject ruleCheck() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_formula_1_0 = null;

        EObject lv_solver_3_0 = null;



        	enterRule();

        try {
            // InternalSat.g:130:2: ( (otherlv_0= 'Check' ( (lv_formula_1_0= ruleBiimplies ) ) otherlv_2= 'using' ( (lv_solver_3_0= ruleSatSolver ) ) otherlv_4= ';' ) )
            // InternalSat.g:131:2: (otherlv_0= 'Check' ( (lv_formula_1_0= ruleBiimplies ) ) otherlv_2= 'using' ( (lv_solver_3_0= ruleSatSolver ) ) otherlv_4= ';' )
            {
            // InternalSat.g:131:2: (otherlv_0= 'Check' ( (lv_formula_1_0= ruleBiimplies ) ) otherlv_2= 'using' ( (lv_solver_3_0= ruleSatSolver ) ) otherlv_4= ';' )
            // InternalSat.g:132:3: otherlv_0= 'Check' ( (lv_formula_1_0= ruleBiimplies ) ) otherlv_2= 'using' ( (lv_solver_3_0= ruleSatSolver ) ) otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getCheckAccess().getCheckKeyword_0());
            		
            // InternalSat.g:136:3: ( (lv_formula_1_0= ruleBiimplies ) )
            // InternalSat.g:137:4: (lv_formula_1_0= ruleBiimplies )
            {
            // InternalSat.g:137:4: (lv_formula_1_0= ruleBiimplies )
            // InternalSat.g:138:5: lv_formula_1_0= ruleBiimplies
            {

            					newCompositeNode(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_5);
            lv_formula_1_0=ruleBiimplies();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCheckRule());
            					}
            					set(
            						current,
            						"formula",
            						lv_formula_1_0,
            						"org.sif.dsl.Sat.Biimplies");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getCheckAccess().getUsingKeyword_2());
            		
            // InternalSat.g:159:3: ( (lv_solver_3_0= ruleSatSolver ) )
            // InternalSat.g:160:4: (lv_solver_3_0= ruleSatSolver )
            {
            // InternalSat.g:160:4: (lv_solver_3_0= ruleSatSolver )
            // InternalSat.g:161:5: lv_solver_3_0= ruleSatSolver
            {

            					newCompositeNode(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_solver_3_0=ruleSatSolver();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCheckRule());
            					}
            					set(
            						current,
            						"solver",
            						lv_solver_3_0,
            						"org.sif.dsl.Sat.SatSolver");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getCheckAccess().getSemicolonKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleSatSolver"
    // InternalSat.g:186:1: entryRuleSatSolver returns [EObject current=null] : iv_ruleSatSolver= ruleSatSolver EOF ;
    public final EObject entryRuleSatSolver() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSatSolver = null;


        try {
            // InternalSat.g:186:50: (iv_ruleSatSolver= ruleSatSolver EOF )
            // InternalSat.g:187:2: iv_ruleSatSolver= ruleSatSolver EOF
            {
             newCompositeNode(grammarAccess.getSatSolverRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSatSolver=ruleSatSolver();

            state._fsp--;

             current =iv_ruleSatSolver; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSatSolver"


    // $ANTLR start "ruleSatSolver"
    // InternalSat.g:193:1: ruleSatSolver returns [EObject current=null] : ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) | ( (lv_name_2_0= 'picosat' ) ) ) ;
    public final EObject ruleSatSolver() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_name_1_0=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalSat.g:199:2: ( ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) | ( (lv_name_2_0= 'picosat' ) ) ) )
            // InternalSat.g:200:2: ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) | ( (lv_name_2_0= 'picosat' ) ) )
            {
            // InternalSat.g:200:2: ( ( (lv_name_0_0= 'SAT4J' ) ) | ( (lv_name_1_0= 'minisat' ) ) | ( (lv_name_2_0= 'picosat' ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 15:
                {
                alt2=2;
                }
                break;
            case 16:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalSat.g:201:3: ( (lv_name_0_0= 'SAT4J' ) )
                    {
                    // InternalSat.g:201:3: ( (lv_name_0_0= 'SAT4J' ) )
                    // InternalSat.g:202:4: (lv_name_0_0= 'SAT4J' )
                    {
                    // InternalSat.g:202:4: (lv_name_0_0= 'SAT4J' )
                    // InternalSat.g:203:5: lv_name_0_0= 'SAT4J'
                    {
                    lv_name_0_0=(Token)match(input,14,FOLLOW_2); 

                    					newLeafNode(lv_name_0_0, grammarAccess.getSatSolverAccess().getNameSAT4JKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSatSolverRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_0, "SAT4J");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSat.g:216:3: ( (lv_name_1_0= 'minisat' ) )
                    {
                    // InternalSat.g:216:3: ( (lv_name_1_0= 'minisat' ) )
                    // InternalSat.g:217:4: (lv_name_1_0= 'minisat' )
                    {
                    // InternalSat.g:217:4: (lv_name_1_0= 'minisat' )
                    // InternalSat.g:218:5: lv_name_1_0= 'minisat'
                    {
                    lv_name_1_0=(Token)match(input,15,FOLLOW_2); 

                    					newLeafNode(lv_name_1_0, grammarAccess.getSatSolverAccess().getNameMinisatKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSatSolverRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_1_0, "minisat");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalSat.g:231:3: ( (lv_name_2_0= 'picosat' ) )
                    {
                    // InternalSat.g:231:3: ( (lv_name_2_0= 'picosat' ) )
                    // InternalSat.g:232:4: (lv_name_2_0= 'picosat' )
                    {
                    // InternalSat.g:232:4: (lv_name_2_0= 'picosat' )
                    // InternalSat.g:233:5: lv_name_2_0= 'picosat'
                    {
                    lv_name_2_0=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(lv_name_2_0, grammarAccess.getSatSolverAccess().getNamePicosatKeyword_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSatSolverRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_2_0, "picosat");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSatSolver"


    // $ANTLR start "entryRuleBiimplies"
    // InternalSat.g:249:1: entryRuleBiimplies returns [EObject current=null] : iv_ruleBiimplies= ruleBiimplies EOF ;
    public final EObject entryRuleBiimplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBiimplies = null;


        try {
            // InternalSat.g:249:50: (iv_ruleBiimplies= ruleBiimplies EOF )
            // InternalSat.g:250:2: iv_ruleBiimplies= ruleBiimplies EOF
            {
             newCompositeNode(grammarAccess.getBiimpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBiimplies=ruleBiimplies();

            state._fsp--;

             current =iv_ruleBiimplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBiimplies"


    // $ANTLR start "ruleBiimplies"
    // InternalSat.g:256:1: ruleBiimplies returns [EObject current=null] : (this_Implies_0= ruleImplies ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) ) )* ) ;
    public final EObject ruleBiimplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Implies_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSat.g:262:2: ( (this_Implies_0= ruleImplies ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) ) )* ) )
            // InternalSat.g:263:2: (this_Implies_0= ruleImplies ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) ) )* )
            {
            // InternalSat.g:263:2: (this_Implies_0= ruleImplies ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) ) )* )
            // InternalSat.g:264:3: this_Implies_0= ruleImplies ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) ) )*
            {

            			newCompositeNode(grammarAccess.getBiimpliesAccess().getImpliesParserRuleCall_0());
            		
            pushFollow(FOLLOW_8);
            this_Implies_0=ruleImplies();

            state._fsp--;


            			current = this_Implies_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:272:3: ( () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalSat.g:273:4: () otherlv_2= '<=>' ( (lv_right_3_0= ruleImplies ) )
            	    {
            	    // InternalSat.g:273:4: ()
            	    // InternalSat.g:274:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,17,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getBiimpliesAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalSat.g:284:4: ( (lv_right_3_0= ruleImplies ) )
            	    // InternalSat.g:285:5: (lv_right_3_0= ruleImplies )
            	    {
            	    // InternalSat.g:285:5: (lv_right_3_0= ruleImplies )
            	    // InternalSat.g:286:6: lv_right_3_0= ruleImplies
            	    {

            	    						newCompositeNode(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_right_3_0=ruleImplies();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getBiimpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.sif.dsl.Sat.Implies");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBiimplies"


    // $ANTLR start "entryRuleImplies"
    // InternalSat.g:308:1: entryRuleImplies returns [EObject current=null] : iv_ruleImplies= ruleImplies EOF ;
    public final EObject entryRuleImplies() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImplies = null;


        try {
            // InternalSat.g:308:48: (iv_ruleImplies= ruleImplies EOF )
            // InternalSat.g:309:2: iv_ruleImplies= ruleImplies EOF
            {
             newCompositeNode(grammarAccess.getImpliesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImplies=ruleImplies();

            state._fsp--;

             current =iv_ruleImplies; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImplies"


    // $ANTLR start "ruleImplies"
    // InternalSat.g:315:1: ruleImplies returns [EObject current=null] : (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* ) ;
    public final EObject ruleImplies() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Or_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSat.g:321:2: ( (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* ) )
            // InternalSat.g:322:2: (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* )
            {
            // InternalSat.g:322:2: (this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )* )
            // InternalSat.g:323:3: this_Or_0= ruleOr ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )*
            {

            			newCompositeNode(grammarAccess.getImpliesAccess().getOrParserRuleCall_0());
            		
            pushFollow(FOLLOW_9);
            this_Or_0=ruleOr();

            state._fsp--;


            			current = this_Or_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:331:3: ( () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==18) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSat.g:332:4: () otherlv_2= '=>' ( (lv_right_3_0= ruleOr ) )
            	    {
            	    // InternalSat.g:332:4: ()
            	    // InternalSat.g:333:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,18,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getImpliesAccess().getEqualsSignGreaterThanSignKeyword_1_1());
            	    			
            	    // InternalSat.g:343:4: ( (lv_right_3_0= ruleOr ) )
            	    // InternalSat.g:344:5: (lv_right_3_0= ruleOr )
            	    {
            	    // InternalSat.g:344:5: (lv_right_3_0= ruleOr )
            	    // InternalSat.g:345:6: lv_right_3_0= ruleOr
            	    {

            	    						newCompositeNode(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_right_3_0=ruleOr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getImpliesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.sif.dsl.Sat.Or");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImplies"


    // $ANTLR start "entryRuleOr"
    // InternalSat.g:367:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // InternalSat.g:367:43: (iv_ruleOr= ruleOr EOF )
            // InternalSat.g:368:2: iv_ruleOr= ruleOr EOF
            {
             newCompositeNode(grammarAccess.getOrRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOr=ruleOr();

            state._fsp--;

             current =iv_ruleOr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // InternalSat.g:374:1: ruleOr returns [EObject current=null] : (this_Excludes_0= ruleExcludes ( () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) ) )* ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Excludes_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSat.g:380:2: ( (this_Excludes_0= ruleExcludes ( () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) ) )* ) )
            // InternalSat.g:381:2: (this_Excludes_0= ruleExcludes ( () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) ) )* )
            {
            // InternalSat.g:381:2: (this_Excludes_0= ruleExcludes ( () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) ) )* )
            // InternalSat.g:382:3: this_Excludes_0= ruleExcludes ( () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrAccess().getExcludesParserRuleCall_0());
            		
            pushFollow(FOLLOW_10);
            this_Excludes_0=ruleExcludes();

            state._fsp--;


            			current = this_Excludes_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:390:3: ( () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==19) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSat.g:391:4: () otherlv_2= 'or' ( (lv_right_3_0= ruleExcludes ) )
            	    {
            	    // InternalSat.g:391:4: ()
            	    // InternalSat.g:392:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrAccess().getOrLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,19,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrAccess().getOrKeyword_1_1());
            	    			
            	    // InternalSat.g:402:4: ( (lv_right_3_0= ruleExcludes ) )
            	    // InternalSat.g:403:5: (lv_right_3_0= ruleExcludes )
            	    {
            	    // InternalSat.g:403:5: (lv_right_3_0= ruleExcludes )
            	    // InternalSat.g:404:6: lv_right_3_0= ruleExcludes
            	    {

            	    						newCompositeNode(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_10);
            	    lv_right_3_0=ruleExcludes();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.sif.dsl.Sat.Excludes");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleExcludes"
    // InternalSat.g:426:1: entryRuleExcludes returns [EObject current=null] : iv_ruleExcludes= ruleExcludes EOF ;
    public final EObject entryRuleExcludes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExcludes = null;


        try {
            // InternalSat.g:426:49: (iv_ruleExcludes= ruleExcludes EOF )
            // InternalSat.g:427:2: iv_ruleExcludes= ruleExcludes EOF
            {
             newCompositeNode(grammarAccess.getExcludesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExcludes=ruleExcludes();

            state._fsp--;

             current =iv_ruleExcludes; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExcludes"


    // $ANTLR start "ruleExcludes"
    // InternalSat.g:433:1: ruleExcludes returns [EObject current=null] : (this_And_0= ruleAnd ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) ) )* ) ;
    public final EObject ruleExcludes() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_And_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalSat.g:439:2: ( (this_And_0= ruleAnd ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) ) )* ) )
            // InternalSat.g:440:2: (this_And_0= ruleAnd ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) ) )* )
            {
            // InternalSat.g:440:2: (this_And_0= ruleAnd ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) ) )* )
            // InternalSat.g:441:3: this_And_0= ruleAnd ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) ) )*
            {

            			newCompositeNode(grammarAccess.getExcludesAccess().getAndParserRuleCall_0());
            		
            pushFollow(FOLLOW_11);
            this_And_0=ruleAnd();

            state._fsp--;


            			current = this_And_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:449:3: ( () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalSat.g:450:4: () otherlv_2= 'nand' ( (lv_right_3_0= ruleAnd ) )
            	    {
            	    // InternalSat.g:450:4: ()
            	    // InternalSat.g:451:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,20,FOLLOW_4); 

            	    				newLeafNode(otherlv_2, grammarAccess.getExcludesAccess().getNandKeyword_1_1());
            	    			
            	    // InternalSat.g:461:4: ( (lv_right_3_0= ruleAnd ) )
            	    // InternalSat.g:462:5: (lv_right_3_0= ruleAnd )
            	    {
            	    // InternalSat.g:462:5: (lv_right_3_0= ruleAnd )
            	    // InternalSat.g:463:6: lv_right_3_0= ruleAnd
            	    {

            	    						newCompositeNode(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    lv_right_3_0=ruleAnd();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getExcludesRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"org.sif.dsl.Sat.And");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExcludes"


    // $ANTLR start "entryRuleAnd"
    // InternalSat.g:485:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // InternalSat.g:485:44: (iv_ruleAnd= ruleAnd EOF )
            // InternalSat.g:486:2: iv_ruleAnd= ruleAnd EOF
            {
             newCompositeNode(grammarAccess.getAndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnd=ruleAnd();

            state._fsp--;

             current =iv_ruleAnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // InternalSat.g:492:1: ruleAnd returns [EObject current=null] : ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        EObject this_Not_0 = null;

        EObject this_Primary_1 = null;

        EObject lv_right_4_1 = null;

        EObject lv_right_4_2 = null;



        	enterRule();

        try {
            // InternalSat.g:498:2: ( ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* ) )
            // InternalSat.g:499:2: ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* )
            {
            // InternalSat.g:499:2: ( (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )* )
            // InternalSat.g:500:3: (this_Not_0= ruleNot | this_Primary_1= rulePrimary ) ( () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )*
            {
            // InternalSat.g:500:3: (this_Not_0= ruleNot | this_Primary_1= rulePrimary )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==22) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID||LA7_0==23) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalSat.g:501:4: this_Not_0= ruleNot
                    {

                    				newCompositeNode(grammarAccess.getAndAccess().getNotParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_12);
                    this_Not_0=ruleNot();

                    state._fsp--;


                    				current = this_Not_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalSat.g:510:4: this_Primary_1= rulePrimary
                    {

                    				newCompositeNode(grammarAccess.getAndAccess().getPrimaryParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_12);
                    this_Primary_1=rulePrimary();

                    state._fsp--;


                    				current = this_Primary_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalSat.g:519:3: ( () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==21) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalSat.g:520:4: () otherlv_3= 'and' ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) )
            	    {
            	    // InternalSat.g:520:4: ()
            	    // InternalSat.g:521:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndAccess().getAndLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_3=(Token)match(input,21,FOLLOW_4); 

            	    				newLeafNode(otherlv_3, grammarAccess.getAndAccess().getAndKeyword_1_1());
            	    			
            	    // InternalSat.g:531:4: ( ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) ) )
            	    // InternalSat.g:532:5: ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) )
            	    {
            	    // InternalSat.g:532:5: ( (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary ) )
            	    // InternalSat.g:533:6: (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary )
            	    {
            	    // InternalSat.g:533:6: (lv_right_4_1= ruleNot | lv_right_4_2= rulePrimary )
            	    int alt8=2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0==22) ) {
            	        alt8=1;
            	    }
            	    else if ( (LA8_0==RULE_ID||LA8_0==23) ) {
            	        alt8=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 8, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt8) {
            	        case 1 :
            	            // InternalSat.g:534:7: lv_right_4_1= ruleNot
            	            {

            	            							newCompositeNode(grammarAccess.getAndAccess().getRightNotParserRuleCall_1_2_0_0());
            	            						
            	            pushFollow(FOLLOW_12);
            	            lv_right_4_1=ruleNot();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getAndRule());
            	            							}
            	            							set(
            	            								current,
            	            								"right",
            	            								lv_right_4_1,
            	            								"org.sif.dsl.Sat.Not");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalSat.g:550:7: lv_right_4_2= rulePrimary
            	            {

            	            							newCompositeNode(grammarAccess.getAndAccess().getRightPrimaryParserRuleCall_1_2_0_1());
            	            						
            	            pushFollow(FOLLOW_12);
            	            lv_right_4_2=rulePrimary();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getAndRule());
            	            							}
            	            							set(
            	            								current,
            	            								"right",
            	            								lv_right_4_2,
            	            								"org.sif.dsl.Sat.Primary");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleNot"
    // InternalSat.g:573:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalSat.g:573:44: (iv_ruleNot= ruleNot EOF )
            // InternalSat.g:574:2: iv_ruleNot= ruleNot EOF
            {
             newCompositeNode(grammarAccess.getNotRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;

             current =iv_ruleNot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalSat.g:580:1: ruleNot returns [EObject current=null] : (otherlv_0= '~' this_Primary_1= rulePrimary () ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_Primary_1 = null;



        	enterRule();

        try {
            // InternalSat.g:586:2: ( (otherlv_0= '~' this_Primary_1= rulePrimary () ) )
            // InternalSat.g:587:2: (otherlv_0= '~' this_Primary_1= rulePrimary () )
            {
            // InternalSat.g:587:2: (otherlv_0= '~' this_Primary_1= rulePrimary () )
            // InternalSat.g:588:3: otherlv_0= '~' this_Primary_1= rulePrimary ()
            {
            otherlv_0=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getNotAccess().getTildeKeyword_0());
            		

            			newCompositeNode(grammarAccess.getNotAccess().getPrimaryParserRuleCall_1());
            		
            pushFollow(FOLLOW_2);
            this_Primary_1=rulePrimary();

            state._fsp--;


            			current = this_Primary_1;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:600:3: ()
            // InternalSat.g:601:4: 
            {

            				current = forceCreateModelElementAndSet(
            					grammarAccess.getNotAccess().getNotRightAction_2(),
            					current);
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRulePrimary"
    // InternalSat.g:611:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalSat.g:611:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalSat.g:612:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalSat.g:618:1: rulePrimary returns [EObject current=null] : (this_Variable_0= ruleVariable | this_PFormula_1= rulePFormula ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        EObject this_Variable_0 = null;

        EObject this_PFormula_1 = null;



        	enterRule();

        try {
            // InternalSat.g:624:2: ( (this_Variable_0= ruleVariable | this_PFormula_1= rulePFormula ) )
            // InternalSat.g:625:2: (this_Variable_0= ruleVariable | this_PFormula_1= rulePFormula )
            {
            // InternalSat.g:625:2: (this_Variable_0= ruleVariable | this_PFormula_1= rulePFormula )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                alt10=1;
            }
            else if ( (LA10_0==23) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalSat.g:626:3: this_Variable_0= ruleVariable
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getVariableParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Variable_0=ruleVariable();

                    state._fsp--;


                    			current = this_Variable_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSat.g:635:3: this_PFormula_1= rulePFormula
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getPFormulaParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_PFormula_1=rulePFormula();

                    state._fsp--;


                    			current = this_PFormula_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleVariable"
    // InternalSat.g:647:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalSat.g:647:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalSat.g:648:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalSat.g:654:1: ruleVariable returns [EObject current=null] : ( (lv_id_0_0= RULE_ID ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;


        	enterRule();

        try {
            // InternalSat.g:660:2: ( ( (lv_id_0_0= RULE_ID ) ) )
            // InternalSat.g:661:2: ( (lv_id_0_0= RULE_ID ) )
            {
            // InternalSat.g:661:2: ( (lv_id_0_0= RULE_ID ) )
            // InternalSat.g:662:3: (lv_id_0_0= RULE_ID )
            {
            // InternalSat.g:662:3: (lv_id_0_0= RULE_ID )
            // InternalSat.g:663:4: lv_id_0_0= RULE_ID
            {
            lv_id_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_id_0_0, grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVariableRule());
            				}
            				setWithLastConsumed(
            					current,
            					"id",
            					lv_id_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRulePFormula"
    // InternalSat.g:682:1: entryRulePFormula returns [EObject current=null] : iv_rulePFormula= rulePFormula EOF ;
    public final EObject entryRulePFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePFormula = null;


        try {
            // InternalSat.g:682:49: (iv_rulePFormula= rulePFormula EOF )
            // InternalSat.g:683:2: iv_rulePFormula= rulePFormula EOF
            {
             newCompositeNode(grammarAccess.getPFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePFormula=rulePFormula();

            state._fsp--;

             current =iv_rulePFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePFormula"


    // $ANTLR start "rulePFormula"
    // InternalSat.g:689:1: rulePFormula returns [EObject current=null] : (otherlv_0= '(' this_Biimplies_1= ruleBiimplies () otherlv_3= ')' ) ;
    public final EObject rulePFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject this_Biimplies_1 = null;



        	enterRule();

        try {
            // InternalSat.g:695:2: ( (otherlv_0= '(' this_Biimplies_1= ruleBiimplies () otherlv_3= ')' ) )
            // InternalSat.g:696:2: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies () otherlv_3= ')' )
            {
            // InternalSat.g:696:2: (otherlv_0= '(' this_Biimplies_1= ruleBiimplies () otherlv_3= ')' )
            // InternalSat.g:697:3: otherlv_0= '(' this_Biimplies_1= ruleBiimplies () otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getPFormulaAccess().getLeftParenthesisKeyword_0());
            		

            			newCompositeNode(grammarAccess.getPFormulaAccess().getBiimpliesParserRuleCall_1());
            		
            pushFollow(FOLLOW_13);
            this_Biimplies_1=ruleBiimplies();

            state._fsp--;


            			current = this_Biimplies_1;
            			afterParserOrEnumRuleCall();
            		
            // InternalSat.g:709:3: ()
            // InternalSat.g:710:4: 
            {

            				current = forceCreateModelElementAndSet(
            					grammarAccess.getPFormulaAccess().getPFormulaExprAction_2(),
            					current);
            			

            }

            otherlv_3=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getPFormulaAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePFormula"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000C00010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001000000L});

}