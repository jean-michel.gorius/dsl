/**
 * generated by Xtext 2.15.0
 */
package org.sif.dsl.sat;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PFormula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.sif.dsl.sat.PFormula#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see org.sif.dsl.sat.SatPackage#getPFormula()
 * @model
 * @generated
 */
public interface PFormula extends Formula
{
  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference.
   * @see #setExpr(Formula)
   * @see org.sif.dsl.sat.SatPackage#getPFormula_Expr()
   * @model containment="true"
   * @generated
   */
  Formula getExpr();

  /**
   * Sets the value of the '{@link org.sif.dsl.sat.PFormula#getExpr <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr</em>' containment reference.
   * @see #getExpr()
   * @generated
   */
  void setExpr(Formula value);

} // PFormula
