/*
 * generated by Xtext 2.15.0
 */
package org.sif.dsl.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.sif.dsl.sat.And;
import org.sif.dsl.sat.Biimplies;
import org.sif.dsl.sat.Check;
import org.sif.dsl.sat.Excludes;
import org.sif.dsl.sat.Formula;
import org.sif.dsl.sat.Implies;
import org.sif.dsl.sat.Not;
import org.sif.dsl.sat.Or;
import org.sif.dsl.sat.PFormula;
import org.sif.dsl.sat.SatPackage;
import org.sif.dsl.sat.SatSolver;
import org.sif.dsl.sat.Start;
import org.sif.dsl.services.SatGrammarAccess;

@SuppressWarnings("all")
public class SatSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private SatGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == SatPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case SatPackage.AND:
				sequence_And(context, (And) semanticObject); 
				return; 
			case SatPackage.BIIMPLIES:
				sequence_Biimplies(context, (Biimplies) semanticObject); 
				return; 
			case SatPackage.CHECK:
				sequence_Check(context, (Check) semanticObject); 
				return; 
			case SatPackage.EXCLUDES:
				sequence_Excludes(context, (Excludes) semanticObject); 
				return; 
			case SatPackage.FORMULA:
				sequence_Variable(context, (Formula) semanticObject); 
				return; 
			case SatPackage.IMPLIES:
				sequence_Implies(context, (Implies) semanticObject); 
				return; 
			case SatPackage.NOT:
				sequence_Not(context, (Not) semanticObject); 
				return; 
			case SatPackage.OR:
				sequence_Or(context, (Or) semanticObject); 
				return; 
			case SatPackage.PFORMULA:
				sequence_PFormula(context, (PFormula) semanticObject); 
				return; 
			case SatPackage.SAT_SOLVER:
				sequence_SatSolver(context, (SatSolver) semanticObject); 
				return; 
			case SatPackage.START:
				sequence_Start(context, (Start) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Biimplies returns And
	 *     Biimplies.Biimplies_1_0 returns And
	 *     Implies returns And
	 *     Implies.Implies_1_0 returns And
	 *     Or returns And
	 *     Or.Or_1_0 returns And
	 *     Excludes returns And
	 *     Excludes.Excludes_1_0 returns And
	 *     And returns And
	 *     And.And_1_0 returns And
	 *     PFormula.PFormula_2 returns And
	 *
	 * Constraint:
	 *     (left=And_And_1_0 (right=Not | right=Primary))
	 */
	protected void sequence_And(ISerializationContext context, And semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns Biimplies
	 *     Biimplies.Biimplies_1_0 returns Biimplies
	 *     PFormula.PFormula_2 returns Biimplies
	 *
	 * Constraint:
	 *     (left=Biimplies_Biimplies_1_0 right=Implies)
	 */
	protected void sequence_Biimplies(ISerializationContext context, Biimplies semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.BIIMPLIES__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.BIIMPLIES__LEFT));
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.BIIMPLIES__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.BIIMPLIES__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getBiimpliesAccess().getBiimpliesLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getBiimpliesAccess().getRightImpliesParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Start.Start_1 returns Check
	 *     Check returns Check
	 *
	 * Constraint:
	 *     (formula=Biimplies solver=SatSolver)
	 */
	protected void sequence_Check(ISerializationContext context, Check semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.CHECK__FORMULA) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.CHECK__FORMULA));
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.CHECK__SOLVER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.CHECK__SOLVER));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getCheckAccess().getFormulaBiimpliesParserRuleCall_1_0(), semanticObject.getFormula());
		feeder.accept(grammarAccess.getCheckAccess().getSolverSatSolverParserRuleCall_3_0(), semanticObject.getSolver());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns Excludes
	 *     Biimplies.Biimplies_1_0 returns Excludes
	 *     Implies returns Excludes
	 *     Implies.Implies_1_0 returns Excludes
	 *     Or returns Excludes
	 *     Or.Or_1_0 returns Excludes
	 *     Excludes returns Excludes
	 *     Excludes.Excludes_1_0 returns Excludes
	 *     PFormula.PFormula_2 returns Excludes
	 *
	 * Constraint:
	 *     (left=Excludes_Excludes_1_0 right=And)
	 */
	protected void sequence_Excludes(ISerializationContext context, Excludes semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.EXCLUDES__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.EXCLUDES__LEFT));
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.EXCLUDES__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.EXCLUDES__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getExcludesAccess().getExcludesLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getExcludesAccess().getRightAndParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns Implies
	 *     Biimplies.Biimplies_1_0 returns Implies
	 *     Implies returns Implies
	 *     Implies.Implies_1_0 returns Implies
	 *     PFormula.PFormula_2 returns Implies
	 *
	 * Constraint:
	 *     (left=Implies_Implies_1_0 right=Or)
	 */
	protected void sequence_Implies(ISerializationContext context, Implies semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.IMPLIES__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.IMPLIES__LEFT));
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.IMPLIES__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.IMPLIES__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getImpliesAccess().getImpliesLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getImpliesAccess().getRightOrParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns Not
	 *     Biimplies.Biimplies_1_0 returns Not
	 *     Implies returns Not
	 *     Implies.Implies_1_0 returns Not
	 *     Or returns Not
	 *     Or.Or_1_0 returns Not
	 *     Excludes returns Not
	 *     Excludes.Excludes_1_0 returns Not
	 *     And returns Not
	 *     And.And_1_0 returns Not
	 *     Not returns Not
	 *     PFormula.PFormula_2 returns Not
	 *
	 * Constraint:
	 *     right=Not_Not_2
	 */
	protected void sequence_Not(ISerializationContext context, Not semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.NOT__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.NOT__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getNotAccess().getNotRightAction_2(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns Or
	 *     Biimplies.Biimplies_1_0 returns Or
	 *     Implies returns Or
	 *     Implies.Implies_1_0 returns Or
	 *     Or returns Or
	 *     Or.Or_1_0 returns Or
	 *     PFormula.PFormula_2 returns Or
	 *
	 * Constraint:
	 *     (left=Or_Or_1_0 right=Excludes)
	 */
	protected void sequence_Or(ISerializationContext context, Or semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.OR__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.OR__LEFT));
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.OR__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.OR__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOrAccess().getOrLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getOrAccess().getRightExcludesParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns PFormula
	 *     Biimplies.Biimplies_1_0 returns PFormula
	 *     Implies returns PFormula
	 *     Implies.Implies_1_0 returns PFormula
	 *     Or returns PFormula
	 *     Or.Or_1_0 returns PFormula
	 *     Excludes returns PFormula
	 *     Excludes.Excludes_1_0 returns PFormula
	 *     And returns PFormula
	 *     And.And_1_0 returns PFormula
	 *     Not.Not_2 returns PFormula
	 *     Primary returns PFormula
	 *     PFormula returns PFormula
	 *     PFormula.PFormula_2 returns PFormula
	 *
	 * Constraint:
	 *     expr=PFormula_PFormula_2
	 */
	protected void sequence_PFormula(ISerializationContext context, PFormula semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.PFORMULA__EXPR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.PFORMULA__EXPR));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getPFormulaAccess().getPFormulaExprAction_2(), semanticObject.getExpr());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     SatSolver returns SatSolver
	 *
	 * Constraint:
	 *     (name='SAT4J' | name='minisat' | name='picosat')
	 */
	protected void sequence_SatSolver(ISerializationContext context, SatSolver semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Start returns Start
	 *
	 * Constraint:
	 *     (checks+=Start_Start_1 checks+=Check*)
	 */
	protected void sequence_Start(ISerializationContext context, Start semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Biimplies returns Formula
	 *     Biimplies.Biimplies_1_0 returns Formula
	 *     Implies returns Formula
	 *     Implies.Implies_1_0 returns Formula
	 *     Or returns Formula
	 *     Or.Or_1_0 returns Formula
	 *     Excludes returns Formula
	 *     Excludes.Excludes_1_0 returns Formula
	 *     And returns Formula
	 *     And.And_1_0 returns Formula
	 *     Not.Not_2 returns Formula
	 *     Primary returns Formula
	 *     Variable returns Formula
	 *     PFormula.PFormula_2 returns Formula
	 *
	 * Constraint:
	 *     id=ID
	 */
	protected void sequence_Variable(ISerializationContext context, Formula semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SatPackage.Literals.FORMULA__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SatPackage.Literals.FORMULA__ID));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVariableAccess().getIdIDTerminalRuleCall_0(), semanticObject.getId());
		feeder.finish();
	}
	
	
}
