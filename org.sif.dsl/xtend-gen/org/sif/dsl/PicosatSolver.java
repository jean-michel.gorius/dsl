package org.sif.dsl;

import com.google.common.base.Objects;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.sif.dsl.Solver;

@SuppressWarnings("all")
public class PicosatSolver extends Solver {
  @Override
  public Boolean check(final String fileContents) {
    try {
      boolean _xblockexpression = false;
      {
        ArrayList<String> commands = new ArrayList<String>();
        commands.add("/bin/sh");
        commands.add("-c");
        commands.add((("echo \"" + fileContents) + "\" | /usr/bin/picosat | awk \'{print $2}\'"));
        final ArrayList<String> _converted_commands = (ArrayList<String>)commands;
        final Process p = Runtime.getRuntime().exec(((String[])Conversions.unwrapArray(_converted_commands, String.class)));
        p.waitFor();
        InputStream _inputStream = p.getInputStream();
        InputStreamReader _inputStreamReader = new InputStreamReader(_inputStream);
        final BufferedReader preader = new BufferedReader(_inputStreamReader);
        final String res = preader.readLine();
        _xblockexpression = Objects.equal(res, "SATISFIABLE");
      }
      return Boolean.valueOf(_xblockexpression);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
