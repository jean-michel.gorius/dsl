package org.sif.dsl;

import java.util.HashMap;
import java.util.Vector;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.sif.dsl.sat.And;
import org.sif.dsl.sat.Biimplies;
import org.sif.dsl.sat.Check;
import org.sif.dsl.sat.Excludes;
import org.sif.dsl.sat.Formula;
import org.sif.dsl.sat.Implies;
import org.sif.dsl.sat.Not;
import org.sif.dsl.sat.Or;
import org.sif.dsl.sat.PFormula;
import org.sif.dsl.sat.SatFactory;

@SuppressWarnings("all")
public abstract class Solver {
  private final EcoreUtil.Copier copier = new EcoreUtil.Copier();
  
  public <T extends EObject> T copy(final T obj) {
    EObject _copy = this.copier.copy(obj);
    return ((T) _copy);
  }
  
  public Vector<Formula> extractClauses(final Formula f) {
    Vector<Formula> _switchResult = null;
    boolean _matched = false;
    if (f instanceof Biimplies) {
      _matched=true;
    }
    if (!_matched) {
      if (f instanceof Implies) {
        _matched=true;
      }
    }
    if (!_matched) {
      if (f instanceof Excludes) {
        _matched=true;
      }
    }
    if (!_matched) {
      if (f instanceof PFormula) {
        _matched=true;
      }
    }
    if (_matched) {
      throw new RuntimeException("Unexpected clause in CNF formula");
    }
    if (!_matched) {
      if (f instanceof Or) {
        _matched=true;
      }
      if (!_matched) {
        if (f instanceof Not) {
          _matched=true;
        }
      }
      if (_matched) {
        Vector<Formula> _xblockexpression = null;
        {
          final Vector<Formula> res = new Vector<Formula>();
          res.add(f);
          _xblockexpression = res;
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof And) {
        _matched=true;
        Vector<Formula> _xblockexpression_1 = null;
        {
          final Vector<Formula> lhs = this.extractClauses(((And)f).getLeft());
          final Vector<Formula> rhs = this.extractClauses(((And)f).getRight());
          final Vector<Formula> res = new Vector<Formula>();
          res.addAll(lhs);
          res.addAll(rhs);
          _xblockexpression_1 = res;
        }
        _switchResult = _xblockexpression_1;
      }
    }
    if (!_matched) {
      {
        String _id = f.getId();
        boolean _tripleNotEquals = (_id != null);
        if (_tripleNotEquals) {
          final Vector<Formula> res = new Vector<Formula>();
          res.add(f);
          return res;
        }
        throw new RuntimeException("Unexpected clause in CNF formula");
      }
    }
    return _switchResult;
  }
  
  public Formula toAnd(final Vector<Formula> clauseList) {
    Formula _xifexpression = null;
    int _size = clauseList.size();
    boolean _lessThan = (_size < 1);
    if (_lessThan) {
      throw new RuntimeException("Empty clause list");
    } else {
      Formula _xifexpression_1 = null;
      int _size_1 = clauseList.size();
      boolean _equals = (_size_1 == 1);
      if (_equals) {
        _xifexpression_1 = clauseList.get(0);
      } else {
        And _xblockexpression = null;
        {
          final And res = SatFactory.eINSTANCE.createAnd();
          res.setLeft(clauseList.remove(0));
          final Formula rhs = this.toAnd(clauseList);
          res.setRight(rhs);
          _xblockexpression = res;
        }
        _xifexpression_1 = _xblockexpression;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public Formula toCNF(final Formula f) {
    Formula _switchResult = null;
    boolean _matched = false;
    if (f instanceof Biimplies) {
      _matched=true;
      Formula _xblockexpression = null;
      {
        final Implies lImpl = SatFactory.eINSTANCE.createImplies();
        lImpl.setLeft(this.<Formula>copy(((Biimplies)f).getLeft()));
        lImpl.setRight(this.<Formula>copy(((Biimplies)f).getRight()));
        final Implies rImpl = SatFactory.eINSTANCE.createImplies();
        rImpl.setLeft(this.<Formula>copy(((Biimplies)f).getRight()));
        rImpl.setRight(this.<Formula>copy(((Biimplies)f).getLeft()));
        final And res = SatFactory.eINSTANCE.createAnd();
        res.setLeft(lImpl);
        res.setRight(rImpl);
        _xblockexpression = this.toCNF(res);
      }
      _switchResult = _xblockexpression;
    }
    if (!_matched) {
      if (f instanceof Implies) {
        _matched=true;
        Formula _xblockexpression = null;
        {
          final Not lNot = SatFactory.eINSTANCE.createNot();
          lNot.setRight(this.<Formula>copy(((Implies)f).getLeft()));
          final Or res = SatFactory.eINSTANCE.createOr();
          res.setLeft(lNot);
          res.setRight(this.<Formula>copy(((Implies)f).getRight()));
          _xblockexpression = this.toCNF(res);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Or) {
        _matched=true;
        Formula _xblockexpression = null;
        {
          final Vector<Formula> lhsClauses = this.extractClauses(this.toCNF(this.<Formula>copy(((Or)f).getLeft())));
          final Vector<Formula> rhsClauses = this.extractClauses(this.toCNF(this.<Formula>copy(((Or)f).getRight())));
          final Vector<Formula> clauses = new Vector<Formula>();
          for (final Formula lc : lhsClauses) {
            for (final Formula rc : rhsClauses) {
              {
                final Or c = SatFactory.eINSTANCE.createOr();
                c.setLeft(this.<Formula>copy(lc));
                c.setRight(this.<Formula>copy(rc));
                clauses.add(c);
              }
            }
          }
          _xblockexpression = this.toAnd(clauses);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Excludes) {
        _matched=true;
        Formula _xblockexpression = null;
        {
          final And and = SatFactory.eINSTANCE.createAnd();
          and.setLeft(this.<Formula>copy(((Excludes)f).getLeft()));
          and.setRight(this.<Formula>copy(((Excludes)f).getRight()));
          final Not res = SatFactory.eINSTANCE.createNot();
          res.setRight(and);
          _xblockexpression = this.toCNF(res);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof And) {
        _matched=true;
        And _xblockexpression = null;
        {
          final Formula lAnd = this.toCNF(this.<Formula>copy(((And)f).getLeft()));
          final Formula rAnd = this.toCNF(this.<Formula>copy(((And)f).getRight()));
          final And res = SatFactory.eINSTANCE.createAnd();
          res.setLeft(lAnd);
          res.setRight(rAnd);
          _xblockexpression = res;
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Not) {
        _matched=true;
        Formula _xblockexpression = null;
        {
          final Formula rhs = ((Not)f).getRight();
          Formula _switchResult_1 = null;
          boolean _matched_1 = false;
          if (rhs instanceof Biimplies) {
            _matched_1=true;
            Formula _xblockexpression_1 = null;
            {
              final And lAnd = SatFactory.eINSTANCE.createAnd();
              lAnd.setLeft(this.<Formula>copy(((Biimplies)rhs).getLeft()));
              lAnd.setRight(this.<Formula>copy(((Biimplies)rhs).getRight()));
              final Not lNot = SatFactory.eINSTANCE.createNot();
              lNot.setRight(this.<Formula>copy(((Biimplies)rhs).getLeft()));
              final Not rNot = SatFactory.eINSTANCE.createNot();
              lNot.setRight(this.<Formula>copy(((Biimplies)rhs).getRight()));
              final And rAnd = SatFactory.eINSTANCE.createAnd();
              rAnd.setLeft(lNot);
              rAnd.setRight(rNot);
              final Or rOr = SatFactory.eINSTANCE.createOr();
              rOr.setLeft(lAnd);
              rOr.setRight(rAnd);
              final Not res = SatFactory.eINSTANCE.createNot();
              res.setRight(rOr);
              _xblockexpression_1 = this.toCNF(res);
            }
            _switchResult_1 = _xblockexpression_1;
          }
          if (!_matched_1) {
            if (rhs instanceof Implies) {
              _matched_1=true;
              Formula _xblockexpression_1 = null;
              {
                final Not lNot = SatFactory.eINSTANCE.createNot();
                lNot.setRight(this.<Formula>copy(((Implies)rhs).getLeft()));
                final Or rOr = SatFactory.eINSTANCE.createOr();
                rOr.setLeft(lNot);
                rOr.setRight(this.<Formula>copy(((Implies)rhs).getRight()));
                final Not res = SatFactory.eINSTANCE.createNot();
                res.setRight(rOr);
                _xblockexpression_1 = this.toCNF(res);
              }
              _switchResult_1 = _xblockexpression_1;
            }
          }
          if (!_matched_1) {
            if (rhs instanceof Or) {
              _matched_1=true;
              Formula _xblockexpression_1 = null;
              {
                final Not lNot = SatFactory.eINSTANCE.createNot();
                lNot.setRight(this.<Formula>copy(((Or)rhs).getLeft()));
                final Not rNot = SatFactory.eINSTANCE.createNot();
                rNot.setRight(this.<Formula>copy(((Or)rhs).getRight()));
                final And res = SatFactory.eINSTANCE.createAnd();
                res.setLeft(lNot);
                res.setRight(rNot);
                _xblockexpression_1 = this.toCNF(res);
              }
              _switchResult_1 = _xblockexpression_1;
            }
          }
          if (!_matched_1) {
            if (rhs instanceof Excludes) {
              _matched_1=true;
              Formula _xblockexpression_1 = null;
              {
                final And res = SatFactory.eINSTANCE.createAnd();
                res.setLeft(this.<Formula>copy(((Excludes)rhs).getLeft()));
                res.setRight(this.<Formula>copy(((Excludes)rhs).getRight()));
                _xblockexpression_1 = this.toCNF(res);
              }
              _switchResult_1 = _xblockexpression_1;
            }
          }
          if (!_matched_1) {
            if (rhs instanceof And) {
              _matched_1=true;
              Formula _xblockexpression_1 = null;
              {
                final Not lNot = SatFactory.eINSTANCE.createNot();
                lNot.setRight(this.<Formula>copy(((And)rhs).getLeft()));
                final Not rNot = SatFactory.eINSTANCE.createNot();
                rNot.setRight(this.<Formula>copy(((And)rhs).getRight()));
                final Or res = SatFactory.eINSTANCE.createOr();
                res.setLeft(lNot);
                res.setRight(rNot);
                _xblockexpression_1 = this.toCNF(res);
              }
              _switchResult_1 = _xblockexpression_1;
            }
          }
          if (!_matched_1) {
            if (rhs instanceof Not) {
              _matched_1=true;
              _switchResult_1 = this.toCNF(this.<Formula>copy(((Not)rhs).getRight()));
            }
          }
          if (!_matched_1) {
            if (rhs instanceof PFormula) {
              _matched_1=true;
              Formula _xblockexpression_1 = null;
              {
                final Not res = SatFactory.eINSTANCE.createNot();
                res.setRight(this.<Formula>copy(((PFormula)rhs).getExpr()));
                _xblockexpression_1 = this.toCNF(res);
              }
              _switchResult_1 = _xblockexpression_1;
            }
          }
          if (!_matched_1) {
            Not _xifexpression = null;
            String _id = rhs.getId();
            boolean _tripleNotEquals = (_id != null);
            if (_tripleNotEquals) {
              _xifexpression = ((Not)f);
            } else {
              throw new RuntimeException("Unexpected error while converting to CNF");
            }
            _switchResult_1 = _xifexpression;
          }
          _xblockexpression = _switchResult_1;
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof PFormula) {
        _matched=true;
        _switchResult = this.toCNF(this.<Formula>copy(((PFormula)f).getExpr()));
      }
    }
    if (!_matched) {
      Formula _xifexpression = null;
      String _id = f.getId();
      boolean _tripleNotEquals = (_id != null);
      if (_tripleNotEquals) {
        _xifexpression = f;
      } else {
        throw new RuntimeException("Unexpected error while converting to CNF");
      }
      _switchResult = _xifexpression;
    }
    return _switchResult;
  }
  
  public HashMap<String, Integer> getVariableIDs(final Formula f, final HashMap<String, Integer> currentIDs) {
    HashMap<String, Integer> _switchResult = null;
    boolean _matched = false;
    if (f instanceof Biimplies) {
      _matched=true;
      HashMap<String, Integer> _xblockexpression = null;
      {
        final HashMap<String, Integer> lhs = this.getVariableIDs(((Biimplies)f).getLeft(), currentIDs);
        currentIDs.putAll(lhs);
        _xblockexpression = this.getVariableIDs(((Biimplies)f).getRight(), currentIDs);
      }
      _switchResult = _xblockexpression;
    }
    if (!_matched) {
      if (f instanceof Implies) {
        _matched=true;
        HashMap<String, Integer> _xblockexpression = null;
        {
          final HashMap<String, Integer> lhs = this.getVariableIDs(((Implies)f).getLeft(), currentIDs);
          currentIDs.putAll(lhs);
          _xblockexpression = this.getVariableIDs(((Implies)f).getRight(), currentIDs);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Or) {
        _matched=true;
        HashMap<String, Integer> _xblockexpression = null;
        {
          final HashMap<String, Integer> lhs = this.getVariableIDs(((Or)f).getLeft(), currentIDs);
          currentIDs.putAll(lhs);
          _xblockexpression = this.getVariableIDs(((Or)f).getRight(), currentIDs);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Excludes) {
        _matched=true;
        HashMap<String, Integer> _xblockexpression = null;
        {
          final HashMap<String, Integer> lhs = this.getVariableIDs(((Excludes)f).getLeft(), currentIDs);
          currentIDs.putAll(lhs);
          _xblockexpression = this.getVariableIDs(((Excludes)f).getRight(), currentIDs);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof And) {
        _matched=true;
        HashMap<String, Integer> _xblockexpression = null;
        {
          final HashMap<String, Integer> lhs = this.getVariableIDs(((And)f).getLeft(), currentIDs);
          currentIDs.putAll(lhs);
          _xblockexpression = this.getVariableIDs(((And)f).getRight(), currentIDs);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Not) {
        _matched=true;
        _switchResult = this.getVariableIDs(((Not)f).getRight(), currentIDs);
      }
    }
    if (!_matched) {
      if (f instanceof PFormula) {
        _matched=true;
        _switchResult = this.getVariableIDs(((PFormula)f).getExpr(), currentIDs);
      }
    }
    if (!_matched) {
      HashMap<String, Integer> _xifexpression = null;
      String _id = f.getId();
      boolean _tripleNotEquals = (_id != null);
      if (_tripleNotEquals) {
        HashMap<String, Integer> _xblockexpression = null;
        {
          boolean _containsKey = currentIDs.containsKey(f.getId());
          boolean _not = (!_containsKey);
          if (_not) {
            String _id_1 = f.getId();
            int _size = currentIDs.size();
            int _plus = (_size + 1);
            currentIDs.put(_id_1, Integer.valueOf(_plus));
          }
          _xblockexpression = currentIDs;
        }
        _xifexpression = _xblockexpression;
      } else {
        throw new RuntimeException("Unexpected formula");
      }
      _switchResult = _xifexpression;
    }
    return _switchResult;
  }
  
  public Integer getNumClauses(final Formula f) {
    Integer _switchResult = null;
    boolean _matched = false;
    if (f instanceof Biimplies) {
      _matched=true;
    }
    if (!_matched) {
      if (f instanceof Implies) {
        _matched=true;
      }
    }
    if (!_matched) {
      if (f instanceof Excludes) {
        _matched=true;
      }
    }
    if (_matched) {
      throw new RuntimeException("Not a CNF formula");
    }
    if (!_matched) {
      if (f instanceof And) {
        _matched=true;
        int _xblockexpression = (int) 0;
        {
          int _switchResult_1 = (int) 0;
          Formula _left = ((And)f).getLeft();
          boolean _matched_1 = false;
          if (_left instanceof And) {
            _matched_1=true;
            _switchResult_1 = 0;
          }
          if (!_matched_1) {
            _switchResult_1 = 1;
          }
          final int lhsContrib = _switchResult_1;
          int _switchResult_2 = (int) 0;
          Formula _right = ((And)f).getRight();
          boolean _matched_2 = false;
          if (_right instanceof And) {
            _matched_2=true;
            _switchResult_2 = 0;
          }
          if (!_matched_2) {
            _switchResult_2 = 1;
          }
          final int rhsContrib = _switchResult_2;
          Integer _numClauses = this.getNumClauses(((And)f).getLeft());
          int _plus = ((lhsContrib + rhsContrib) + (_numClauses).intValue());
          Integer _numClauses_1 = this.getNumClauses(((And)f).getRight());
          _xblockexpression = (_plus + (_numClauses_1).intValue());
        }
        _switchResult = Integer.valueOf(_xblockexpression);
      }
    }
    if (!_matched) {
      if (f instanceof PFormula) {
        _matched=true;
        _switchResult = this.getNumClauses(((PFormula)f).getExpr());
      }
    }
    if (!_matched) {
      _switchResult = Integer.valueOf(0);
    }
    return _switchResult;
  }
  
  public String satToDimacs(final Formula f, final HashMap<String, Integer> variableIDs) {
    String _switchResult = null;
    boolean _matched = false;
    if (f instanceof Biimplies) {
      _matched=true;
    }
    if (!_matched) {
      if (f instanceof Implies) {
        _matched=true;
      }
    }
    if (!_matched) {
      if (f instanceof Excludes) {
        _matched=true;
      }
    }
    if (_matched) {
      throw new RuntimeException("Not a CNF formula");
    }
    if (!_matched) {
      if (f instanceof Or) {
        _matched=true;
        String _xblockexpression = null;
        {
          String lhs = this.satToDimacs(((Or)f).getLeft(), variableIDs);
          String rhs = this.satToDimacs(((Or)f).getRight(), variableIDs);
          _xblockexpression = ((lhs + " ") + rhs);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof And) {
        _matched=true;
        String _xblockexpression = null;
        {
          String lhs = this.satToDimacs(((And)f).getLeft(), variableIDs);
          String rhs = this.satToDimacs(((And)f).getRight(), variableIDs);
          String _xifexpression = null;
          Formula _left = ((And)f).getLeft();
          if ((_left instanceof And)) {
            _xifexpression = lhs;
          } else {
            _xifexpression = (lhs + " 0\n");
          }
          String lhsStr = _xifexpression;
          String _xifexpression_1 = null;
          Formula _right = ((And)f).getRight();
          if ((_right instanceof And)) {
            _xifexpression_1 = rhs;
          } else {
            _xifexpression_1 = (rhs + " 0\n");
          }
          String rhsStr = _xifexpression_1;
          _xblockexpression = (lhsStr + rhsStr);
        }
        _switchResult = _xblockexpression;
      }
    }
    if (!_matched) {
      if (f instanceof Not) {
        _matched=true;
        String _satToDimacs = this.satToDimacs(((Not)f).getRight(), variableIDs);
        _switchResult = ("-" + _satToDimacs);
      }
    }
    if (!_matched) {
      if (f instanceof PFormula) {
        _matched=true;
        _switchResult = this.satToDimacs(((PFormula)f).getExpr(), variableIDs);
      }
    }
    if (!_matched) {
      String _xifexpression = null;
      String _id = f.getId();
      boolean _tripleNotEquals = (_id != null);
      if (_tripleNotEquals) {
        _xifexpression = variableIDs.get(f.getId()).toString();
      } else {
        throw new RuntimeException("Unexpected formula");
      }
      _switchResult = _xifexpression;
    }
    return _switchResult;
  }
  
  public String checkSatisfiability(final Check c) {
    String _xblockexpression = null;
    {
      final Formula cnfFormula = this.toCNF(c.getFormula());
      HashMap<String, Integer> _hashMap = new HashMap<String, Integer>();
      final HashMap<String, Integer> variableIDs = this.getVariableIDs(cnfFormula, _hashMap);
      final int numVariables = variableIDs.size();
      final Integer numClauses = this.getNumClauses(cnfFormula);
      final String dimacsStr = this.satToDimacs(cnfFormula, variableIDs);
      Integer _xifexpression = null;
      if (((numClauses).intValue() > 0)) {
        _xifexpression = numClauses;
      } else {
        _xifexpression = Integer.valueOf(1);
      }
      String _format = String.format("p cnf %d %d\n", Integer.valueOf(numVariables), _xifexpression);
      final String dimacsFileContents = (_format + dimacsStr);
      String _xifexpression_1 = null;
      Boolean _check = this.check(dimacsFileContents);
      if ((_check).booleanValue()) {
        _xifexpression_1 = "SATISFIABLE";
      } else {
        _xifexpression_1 = "UNSATISFIABLE";
      }
      _xblockexpression = _xifexpression_1;
    }
    return _xblockexpression;
  }
  
  public abstract Boolean check(final String fileContents);
}
