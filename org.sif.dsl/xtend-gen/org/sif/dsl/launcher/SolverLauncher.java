package org.sif.dsl.launcher;

import com.google.common.base.Objects;
import com.google.inject.Injector;
import java.io.ByteArrayInputStream;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.sif.dsl.MinisatSolver;
import org.sif.dsl.PicosatSolver;
import org.sif.dsl.SAT4JSolver;
import org.sif.dsl.SatStandaloneSetup;
import org.sif.dsl.sat.And;
import org.sif.dsl.sat.Biimplies;
import org.sif.dsl.sat.Check;
import org.sif.dsl.sat.Excludes;
import org.sif.dsl.sat.Formula;
import org.sif.dsl.sat.Implies;
import org.sif.dsl.sat.Not;
import org.sif.dsl.sat.Or;
import org.sif.dsl.sat.PFormula;
import org.sif.dsl.sat.Start;

@SuppressWarnings("all")
public class SolverLauncher {
  private final Injector injector = new SatStandaloneSetup().createInjectorAndDoEMFRegistration();
  
  public static MessageConsole findConsole(final String name) {
    ConsolePlugin _default = ConsolePlugin.getDefault();
    boolean _tripleEquals = (_default == null);
    if (_tripleEquals) {
      return null;
    }
    final ConsolePlugin plugin = ConsolePlugin.getDefault();
    final IConsoleManager conMan = plugin.getConsoleManager();
    final IConsole[] existing = conMan.getConsoles();
    for (int i = 0; (i < existing.length); i++) {
      boolean _equals = name.equals(existing[i].getName());
      if (_equals) {
        conMan.showConsoleView(existing[i]);
        IConsole _get = existing[i];
        return ((MessageConsole) _get);
      }
    }
    final MessageConsole myConsole = new MessageConsole(name, null);
    conMan.addConsoles(new IConsole[] { myConsole });
    return myConsole;
  }
  
  public MessageConsoleStream getMessageStream() {
    final MessageConsole myConsole = SolverLauncher.findConsole("Satisfiability check");
    if ((myConsole != null)) {
      final IWorkbench wb = PlatformUI.getWorkbench();
      final IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
      final IWorkbenchPage page = win.getActivePage();
      final String id = IConsoleConstants.ID_CONSOLE_VIEW;
      IConsoleView view = ((IConsoleView) null);
      try {
        IViewPart _showView = page.showView(id);
        view = ((IConsoleView) _showView);
        view.display(myConsole);
        return myConsole.newMessageStream();
      } catch (final Throwable _t) {
        if (_t instanceof PartInitException) {
          final PartInitException e = (PartInitException)_t;
          e.printStackTrace();
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    }
    return null;
  }
  
  public String toString(final Formula f) {
    String _switchResult = null;
    boolean _matched = false;
    if (f instanceof Biimplies) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("(");
      String _string = this.toString(((Biimplies)f).getLeft());
      _builder.append(_string);
      _builder.append(" <=> ");
      String _string_1 = this.toString(((Biimplies)f).getRight());
      _builder.append(_string_1);
      _builder.append(")");
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      if (f instanceof Implies) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        String _string = this.toString(((Implies)f).getLeft());
        _builder.append(_string);
        _builder.append(" => ");
        String _string_1 = this.toString(((Implies)f).getRight());
        _builder.append(_string_1);
        _builder.append(")");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (f instanceof Or) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        String _string = this.toString(((Or)f).getLeft());
        _builder.append(_string);
        _builder.append(" || ");
        String _string_1 = this.toString(((Or)f).getRight());
        _builder.append(_string_1);
        _builder.append(")");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (f instanceof Excludes) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        String _string = this.toString(((Excludes)f).getLeft());
        _builder.append(_string);
        _builder.append(" ~& ");
        String _string_1 = this.toString(((Excludes)f).getRight());
        _builder.append(_string_1);
        _builder.append(")");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (f instanceof And) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        String _string = this.toString(((And)f).getLeft());
        _builder.append(_string);
        _builder.append(" && ");
        String _string_1 = this.toString(((And)f).getRight());
        _builder.append(_string_1);
        _builder.append(")");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (f instanceof Not) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("~");
        String _string = this.toString(((Not)f).getRight());
        _builder.append(_string);
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (f instanceof PFormula) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("(");
        String _string = this.toString(((PFormula)f).getExpr());
        _builder.append(_string);
        _builder.append(")");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      String _xifexpression = null;
      String _id = f.getId();
      boolean _tripleNotEquals = (_id != null);
      if (_tripleNotEquals) {
        _xifexpression = f.getId();
      } else {
        throw new RuntimeException("Unexpected expression");
      }
      _switchResult = _xifexpression;
    }
    return _switchResult;
  }
  
  public void launch(final String fileContents) {
    try {
      final XtextResourceSet resourceSet = this.injector.<XtextResourceSet>getInstance(XtextResourceSet.class);
      resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
      final Resource resource = resourceSet.createResource(URI.createURI("dummy:/file.sat"));
      byte[] _bytes = fileContents.getBytes();
      final ByteArrayInputStream in = new ByteArrayInputStream(_bytes);
      resource.load(in, resourceSet.getLoadOptions());
      EObject _get = resource.getContents().get(0);
      final Start result = ((Start) _get);
      final MessageConsoleStream outStream = this.getMessageStream();
      final SAT4JSolver sat4jSolver = new SAT4JSolver();
      final MinisatSolver minisatSolver = new MinisatSolver();
      final PicosatSolver picosatSolver = new PicosatSolver();
      EList<Check> _checks = result.getChecks();
      for (final Check check : _checks) {
        {
          String _string = this.toString(check.getFormula());
          String _plus = ("Checking satisfiability of " + _string);
          String _plus_1 = (_plus + " [");
          String _name = check.getSolver().getName();
          String _plus_2 = (_plus_1 + _name);
          String _plus_3 = (_plus_2 + "]");
          outStream.println(_plus_3);
          String _name_1 = check.getSolver().getName();
          boolean _equals = Objects.equal(_name_1, "SAT4J");
          if (_equals) {
            outStream.println(sat4jSolver.checkSatisfiability(check));
          } else {
            String _name_2 = check.getSolver().getName();
            boolean _equals_1 = Objects.equal(_name_2, "minisat");
            if (_equals_1) {
              outStream.println(minisatSolver.checkSatisfiability(check));
            } else {
              String _name_3 = check.getSolver().getName();
              boolean _equals_2 = Objects.equal(_name_3, "picosat");
              if (_equals_2) {
                outStream.println(picosatSolver.checkSatisfiability(check));
              } else {
                String _name_4 = check.getSolver().getName();
                String _plus_4 = ("Unknown solver " + _name_4);
                outStream.println(_plus_4);
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
