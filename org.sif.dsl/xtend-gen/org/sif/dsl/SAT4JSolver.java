package org.sif.dsl;

import java.io.ByteArrayInputStream;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sif.dsl.Solver;

@SuppressWarnings("all")
public class SAT4JSolver extends Solver {
  @Override
  public Boolean check(final String fileContents) {
    try {
      boolean _xblockexpression = false;
      {
        final ISolver solver = SolverFactory.newDefault();
        solver.setTimeout(3600);
        final DimacsReader reader = new DimacsReader(solver);
        boolean _xtrycatchfinallyexpression = false;
        try {
          boolean _xblockexpression_1 = false;
          {
            byte[] _bytes = fileContents.getBytes("US-ASCII");
            ByteArrayInputStream _byteArrayInputStream = new ByteArrayInputStream(_bytes);
            final IProblem problem = reader.parseInstance(_byteArrayInputStream);
            boolean _xifexpression = false;
            boolean _isSatisfiable = problem.isSatisfiable();
            if (_isSatisfiable) {
              _xifexpression = true;
            } else {
              _xifexpression = false;
            }
            _xblockexpression_1 = _xifexpression;
          }
          _xtrycatchfinallyexpression = _xblockexpression_1;
        } catch (final Throwable _t) {
          if (_t instanceof ContradictionException) {
            _xtrycatchfinallyexpression = false;
          } else if (_t instanceof TimeoutException) {
            _xtrycatchfinallyexpression = false;
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        _xblockexpression = _xtrycatchfinallyexpression;
      }
      return Boolean.valueOf(_xblockexpression);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
